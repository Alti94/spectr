#ifndef CSVHANDLER_H
#define CSVHANDLER_H

#include <QtCore>

class CsvHandler
{
public:
    CsvHandler();
    bool loadCsv(const QString &filePath);
    bool saveCsv(const QString &filePath);

    QVector<QPointF> toDataVector(bool *success = nullptr);
    void toCsv(const QVector<QPointF> &data);

    QChar fieldSeparator() const;
    void setFieldSeparator(const QChar &fieldSeparator);

    QChar textSeparator() const;
    void setTextSeparator(const QChar &textSeparator);

    QList<QStringList> data() const;
    void setData(const QList<QStringList> &data);

private:
    QChar m_fieldSeparator;
    QChar m_textSeparator;
    QList<QStringList> m_data;

    QStringList parseLine(const QString &lineToParse);
    QString removeDoubleTextSerparator(QString &field);
    QString prepareLineToWrite(QStringList list);
};

#endif // CSVHANDLER_H
