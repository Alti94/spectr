#include "ciecri.h"
#include "samplesdata.h"
#include "chromaticadapt.h"

#define CRI_SAMPLE_COUNT 14

CieCRI::CieCRI()
{
}

CieCRI::CieCRI(const Chromacity &testChroma) :
    m_testChroma(testChroma),
    m_testSamXYZ(QVector<CIE_XYZ>(CRI_SAMPLE_COUNT, {0, 0, 0})),
    m_refSamXYZ(QVector<CIE_XYZ>(CRI_SAMPLE_COUNT, {0, 0, 0})),
    m_testSamUv60C(QVector<UV60_CHROMA>(CRI_SAMPLE_COUNT)),
    m_refSamUv60C(QVector<UV60_CHROMA>(CRI_SAMPLE_COUNT)),
    m_testSamWUVs(QVector<CieWUV>(CRI_SAMPLE_COUNT)),
    m_refSamWUVs(QVector<CieWUV>(CRI_SAMPLE_COUNT)),
    m_ri(QVector<qreal>(CRI_SAMPLE_COUNT))
{
    selectReference();
    countSamplesChroma();
    chromaAdaptTestUv60();
    countWUVs();
    countRa();
}

CieCRI::CieCRI(const CieCRI &obj) :
    m_testSamWUVs(obj.m_testSamWUVs),
    m_refSamWUVs(obj.m_testSamWUVs),
    m_ri(obj.m_ri),
    m_ra(obj.m_ra)
{
}

QVector<CieWUV> CieCRI::testSamWUVs() const
{
    return m_testSamWUVs;
}

QVector<CieWUV> CieCRI::refSamWUVs() const
{
    return m_refSamWUVs;
}

QVector<qreal> CieCRI::ri() const
{
    return m_ri;
}

qreal CieCRI::ra() const
{
    return m_ra;
}

Chromacity CieCRI::testChroma() const
{
    return m_testChroma;
}

Chromacity CieCRI::referenceChroma() const
{
    return m_referenceChroma;
}

QVector<CIE_XYZ> CieCRI::testSamXYZ() const
{
    return m_testSamXYZ;
}

QVector<CIE_XYZ> CieCRI::refSamXYZ() const
{
    return m_refSamXYZ;
}

void CieCRI::selectReference()
{
    qreal testTemp = m_testChroma.temperature().temp();
    if (testTemp < 5000)
        m_referenceChroma = Chromacity(SamplesData::planckianWave(testTemp));
    else
        m_referenceChroma = Chromacity(SamplesData::daylightWave(testTemp));
}

void CieCRI::countSamplesChroma()
{
    QVector<qreal> testWave = m_testChroma.yNormWave();
    QVector<qreal> refWave = m_referenceChroma.yNormWave();

    for (int i = 0; i < 14; i++)
    {
        for (int j = 0; j < 95; j++)
        {
            m_testSamXYZ[i].X += testWave[j] * SamplesData::cmfXYZ_2[j].x * SamplesData::samplesTCS[i][j];
            m_testSamXYZ[i].Y += testWave[j] * SamplesData::cmfXYZ_2[j].y * SamplesData::samplesTCS[i][j];
            m_testSamXYZ[i].Z += testWave[j] * SamplesData::cmfXYZ_2[j].z * SamplesData::samplesTCS[i][j];

            m_refSamXYZ[i].X += refWave[j] * SamplesData::cmfXYZ_2[j].x * SamplesData::samplesTCS[i][j];
            m_refSamXYZ[i].Y += refWave[j] * SamplesData::cmfXYZ_2[j].y * SamplesData::samplesTCS[i][j];
            m_refSamXYZ[i].Z += refWave[j] * SamplesData::cmfXYZ_2[j].z * SamplesData::samplesTCS[i][j];
        }

        m_testSamUv60C[i] = Chromacity::toUv60Chroma(m_testSamXYZ[i]);
        m_refSamUv60C[i] = Chromacity::toUv60Chroma(m_refSamXYZ[i]);
    }
}

void CieCRI::chromaAdaptTestUv60()
{
    UV60_CHROMA testUv = m_testChroma.uv60Chroma();
    UV60_CHROMA refUv = m_referenceChroma.uv60Chroma();

    qreal ck = (4 - testUv.u - 10 * testUv.v) / testUv.v;
    qreal dk = (1.708 * testUv.v + 0.404 - 1.481 * testUv.u) / testUv.v;
    qreal cr = (4 - refUv.u - 10 * refUv.v) / refUv.v;
    qreal dr = (1.708 * refUv.v + 0.404 - 1.481 * refUv.u) / refUv.v;

    for (int i = 0; i < 14; i++)
    {
        qreal cki = (4 - m_testSamUv60C[i].u - 10 * m_testSamUv60C[i].v) / m_testSamUv60C[i].v;
        qreal dki = (1.708 * m_testSamUv60C[i].v + 0.404 - 1.481 * m_testSamUv60C[i].u) / m_testSamUv60C[i].v;

        m_testSamUv60C[i].u = (10.872 + 0.404 * cr / ck * cki - 4 * dr / dk * dki) / (16.518 + 1.481 * cr / ck * cki - dr / dk * dki);
        m_testSamUv60C[i].v = 5.52 / (16.518 + 1.481 * cr / ck * cki - dr / dk * dki);
    }

    qreal cki = (4 - testUv.u - 10 * testUv.v) / testUv.v;
    qreal dki = (1.708 * testUv.v + 0.404 - 1.481 * testUv.u) / testUv.v;

    m_testChromaAdapted.u = (10.872 + 0.404 * cr / ck * cki - 4 * dr / dk * dki) / (16.518 + 1.481 * cr / ck * cki - dr / dk * dki);
    m_testChromaAdapted.v = 5.52 / (16.518 + 1.481 * cr / ck * cki - dr / dk * dki);
}

void CieCRI::countWUVs()
{
    for (int i = 0; i < 14; i++)
    {
        m_testSamWUVs[i] = toWUV(m_testSamUv60C[i], m_testSamXYZ[i].Y, m_testChromaAdapted);
        m_refSamWUVs[i] = toWUV(m_refSamUv60C[i], m_refSamXYZ[i].Y, m_referenceChroma.uv60Chroma());
    }
}

CieWUV CieCRI::toWUV(UV60_CHROMA sampleUV60, qreal sampleY, UV60_CHROMA srcUV60)
{
    CieWUV wuv;
    wuv.W = 25 * qPow(sampleY, (1.0 / 3.0)) - 17;
    wuv.U = 13 * wuv.W * (sampleUV60.u - srcUV60.u);
    wuv.V = 13 * wuv.W * (sampleUV60.v - srcUV60.v);

    return wuv;
}

void CieCRI::countRa()
{
    for (int i = 0; i < 14; i++)
    {
        qreal dEi = qSqrt(qPow(m_refSamWUVs[i].U - m_testSamWUVs[i].U, 2) + qPow(m_refSamWUVs[i].V - m_testSamWUVs[i].V, 2) + qPow(m_refSamWUVs[i].W - m_testSamWUVs[i].W, 2));
        m_ri[i] = 100 - 4.6 * dEi;
    }

    m_ra = 0;
    for (int i = 0; i < 8; i++)
        m_ra += m_ri[i];

    m_ra /= 8;
}
