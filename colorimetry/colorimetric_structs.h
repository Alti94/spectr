#ifndef COLORYMETRIC_STRUCTS_H
#define COLORYMETRIC_STRUCTS_H

#include <QtCore>

struct CMF_XYZ
{
    qreal x;
    qreal y;
    qreal z;
};

struct CIE_XYZ
{
    qreal X;
    qreal Y;
    qreal Z;
};

struct CIE_RGB
{
    qreal R;
    qreal G;
    qreal B;
};

struct XYZ_CHROMA
{
    qreal x;
    qreal y;
    qreal z;
};

struct UV60_CHROMA
{
    qreal u;
    qreal v;
};

struct UV76_CHROMA
{
    qreal uP;
    qreal vP;
};

struct CieLAB
{
    qreal L;
    qreal a;
    qreal b;
};

struct Jab
{
    qreal J;
    qreal a;
    qreal b;
};

struct CieWUV
{
    qreal W;
    qreal U;
    qreal V;
};

#endif // COLORYMETRIC_STRUCTS_H
