#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#include <QtCore>
#include "colorimetric_structs.h"

class Temperature
{
public:
    Temperature();
    Temperature(const UV60_CHROMA &uv60chroma);
    Temperature(const Temperature &obj);
    qreal temp() const;
    qreal duv() const;

private:
    UV60_CHROMA m_uv;
    qreal m_temp;
    qreal m_duv;
    QVector<qreal> m_distance;
    int m_minPos;
    qreal m_sign;

    void countTemperature();
    void triangularMethod();
    void parabolicMethod();
    qreal countDistance(qreal x1, qreal x2, qreal y1, qreal y2);
    qreal sgn(qreal z);

    struct PLANCK_TUV
    {
        qreal temp;
        qreal u;
        qreal v;
    };

    static const PLANCK_TUV planckTab[303];
};

#endif // TEMPERATURE_H
