#ifndef CIECRI_H
#define CIECRI_H

#include <QtCore>
#include "chromacity.h"

class CieCRI
{
public:
    CieCRI();
    CieCRI(const Chromacity &testChroma);
    CieCRI(const CieCRI &obj);


    QVector<CieWUV> testSamWUVs() const;
    QVector<CieWUV> refSamWUVs() const;

    QVector<qreal> ri() const;
    qreal ra() const;

    Chromacity testChroma() const;
    Chromacity referenceChroma() const;

    QVector<CIE_XYZ> testSamXYZ() const;
    QVector<CIE_XYZ> refSamXYZ() const;

private:
    Chromacity m_testChroma;
    Chromacity m_referenceChroma;
    QVector<CIE_XYZ> m_testSamXYZ, m_refSamXYZ;
    QVector<UV60_CHROMA> m_testSamUv60C, m_refSamUv60C;
    UV60_CHROMA m_testChromaAdapted;
    QVector<CieWUV> m_testSamWUVs, m_refSamWUVs;
    QVector<qreal> m_ri;
    qreal m_ra;


    void selectReference();
    void countSamplesChroma();
    void chromaAdaptTestUv60(); //von Kries
    void countWUVs();
    CieWUV toWUV(UV60_CHROMA sampleUV60, qreal sampleY, UV60_CHROMA srcUV60);
    void countRa();


};

#endif // CIECRI_H
