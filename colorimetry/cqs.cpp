#include "cqs.h"

#include "samplesdata.h"

#define CQS_SAMPLE_COUNT 15

Cqs::Cqs()
{
}

Cqs::Cqs(const Chromacity &testChroma) :
    m_testChroma(testChroma),
    m_testSamXYZ(QVector<CIE_XYZ>(CQS_SAMPLE_COUNT)),
    m_refSamXYZ(QVector<CIE_XYZ>(CQS_SAMPLE_COUNT)),
    m_testSamAdXYZ(QVector<CIE_XYZ>(CQS_SAMPLE_COUNT)),
    m_testSamLabs(QVector<CieLAB>(CQS_SAMPLE_COUNT)),
    m_refSamLabs(QVector<CieLAB>(CQS_SAMPLE_COUNT)),
    m_cTest(QVector<qreal>(CQS_SAMPLE_COUNT)),
    m_cRef(QVector<qreal>(CQS_SAMPLE_COUNT)),
    m_deltaC(QVector<qreal>(CQS_SAMPLE_COUNT)),
    m_deltaE(QVector<qreal>(CQS_SAMPLE_COUNT)),
    m_deltaEsat(QVector<qreal>(CQS_SAMPLE_COUNT)),
    m_chromaRatio(QVector<qreal>(CQS_SAMPLE_COUNT)),
    m_qi(QVector<qreal>(CQS_SAMPLE_COUNT))
{
    selectReference();
    countSamplesChroma();
    chromaAdaptTestXyz();
    countLabs();
    countDeltaEsat();
    countDeltaErms();
    countScalFact();
    countCCTFactor();
    countChromaRatio();
    countQa();
    countQi();
}

Cqs::Cqs(const Cqs &obj)
{

}

Chromacity Cqs::testChroma() const
{
    return m_testChroma;
}

Chromacity Cqs::referenceChroma() const
{
    return m_referenceChroma;
}

QVector<CIE_XYZ> Cqs::testSamXYZ() const
{
    return m_testSamXYZ;
}

QVector<CIE_XYZ> Cqs::refSamXYZ() const
{
    return m_refSamXYZ;
}

QVector<qreal> Cqs::qi() const
{
    return m_qi;
}

qreal Cqs::qa() const
{
    return m_qa;
}

QVector<qreal> Cqs::chromaRatio() const
{
    return m_chromaRatio;
}

void Cqs::selectReference()
{
    qreal testTemp = m_testChroma.temperature().temp();
    if (testTemp < 5000)
        m_referenceChroma = Chromacity(SamplesData::planckianWave(testTemp));
    else
        m_referenceChroma = Chromacity(SamplesData::daylightWave(testTemp));
}

void Cqs::countSamplesChroma()
{
    QVector<qreal> testWave = m_testChroma.yNormWave();
    QVector<qreal> refWave = m_referenceChroma.yNormWave();

    for (int i = 0; i < CQS_SAMPLE_COUNT; i++)
    {
        m_testSamXYZ[i] = { (qreal) 0, (qreal) 0, (qreal) 0 };
        m_refSamXYZ[i] = { (qreal) 0, (qreal) 0, (qreal) 0 };

        for (int j = 0; j < 95; j++)
        {
            m_testSamXYZ[i].X += testWave[j] * SamplesData::cmfXYZ_2[j].x * SamplesData::samplesVS[i][j];
            m_testSamXYZ[i].Y += testWave[j] * SamplesData::cmfXYZ_2[j].y * SamplesData::samplesVS[i][j];
            m_testSamXYZ[i].Z += testWave[j] * SamplesData::cmfXYZ_2[j].z * SamplesData::samplesVS[i][j];

            m_refSamXYZ[i].X += refWave[j] * SamplesData::cmfXYZ_2[j].x * SamplesData::samplesVS[i][j];
            m_refSamXYZ[i].Y += refWave[j] * SamplesData::cmfXYZ_2[j].y * SamplesData::samplesVS[i][j];
            m_refSamXYZ[i].Z += refWave[j] * SamplesData::cmfXYZ_2[j].z * SamplesData::samplesVS[i][j];
        }
    }
}

void Cqs::chromaAdaptTestXyz()
{
    CIE_RGB testRGB = toRGB(m_testChroma.xyz());
    CIE_RGB refRGB = toRGB(m_referenceChroma.xyz());
    QVector<CIE_RGB> samplesRGB(QVector<CIE_RGB>(15));

    for (int i = 0; i < CQS_SAMPLE_COUNT; i++)
        samplesRGB[i] = toRGB(m_testSamXYZ[i]);

    qreal alpha = m_testChroma.xyz().Y / m_referenceChroma.xyz().Y;
    qreal r = (refRGB.R / testRGB.R) * alpha;
    qreal g = (refRGB.G / testRGB.G) * alpha;
    qreal b = (refRGB.B / testRGB.B) * alpha;

    for (int i = 0; i < CQS_SAMPLE_COUNT; i++)
    {
        samplesRGB[i].R *= r;
        samplesRGB[i].G *= g;
        samplesRGB[i].B *= b;

        m_testSamAdXYZ[i] = toXYZ(samplesRGB[i]);
    }

    testRGB.R *= r;
    testRGB.G *= g;
    testRGB.B *= b;

    m_testAdXYZ = toXYZ(testRGB);
}

CIE_RGB Cqs::toRGB(const CIE_XYZ &xyz)
{
    const qreal mat[3][3] = {
        { 0.7982, 0.3389, -0.1371 },
        { -0.5918, 1.5512, 0.0406 },
        { 0.0008, 0.0239, 0.9753 },
    };

    CIE_RGB rgb;
    rgb.R = xyz.X * mat[0][0] + xyz.Y * mat[0][1] + xyz.Z * mat[0][2];
    rgb.G = xyz.X * mat[1][0] + xyz.Y * mat[1][1] + xyz.Z * mat[1][2];
    rgb.B = xyz.X * mat[2][0] + xyz.Y * mat[2][1] + xyz.Z * mat[2][2];

    return rgb;
}

CIE_XYZ Cqs::toXYZ(const CIE_RGB &rgb)
{
    const qreal mat[3][3] = {
        { 1.076450049, -0.237662388, 0.161212339 },
        { 0.410964325, 0.554341804, 0.03469387 },
        { -0.010953765, -0.013389356, 1.024343122 },
    };

    CIE_XYZ xyz;
    xyz.X = rgb.R * mat[0][0] + rgb.G * mat[0][1] + rgb.B * mat[0][2];
    xyz.Y = rgb.R * mat[1][0] + rgb.G * mat[1][1] + rgb.B * mat[1][2];
    xyz.Z = rgb.R * mat[2][0] + rgb.G * mat[2][1] + rgb.B * mat[2][2];

    return xyz;
}

void Cqs::countLabs()
{
    for (int i = 0; i < CQS_SAMPLE_COUNT; i++)
    {
        m_testSamLabs[i] = toLab(m_testAdXYZ, m_testSamAdXYZ[i]);
        m_cTest[i] = qSqrt(qPow(m_testSamLabs[i].a, 2) + qPow(m_testSamLabs[i].b, 2));

        m_refSamLabs[i] = toLab(m_referenceChroma.xyz(), m_refSamXYZ[i]);
        m_cRef[i] = qSqrt(qPow(m_refSamLabs[i].a, 2) + qPow(m_refSamLabs[i].b, 2));

        m_deltaC[i] = m_cTest[i] - m_cRef[i];
        m_deltaE[i] = qSqrt(qPow(m_testSamLabs[i].L - m_refSamLabs[i].L, 2) +
                            qPow(m_testSamLabs[i].a - m_refSamLabs[i].a, 2) +
                            qPow(m_testSamLabs[i].b - m_refSamLabs[i].b, 2));
    }
}

CieLAB Cqs::toLab(const CIE_XYZ &ref, const CIE_XYZ &sample)
{
    CieLAB lab;
    lab.L = 116 * qPow(sample.Y / ref.Y, (1.0 / 3.0)) - 16;
    lab.a = 500 * (qPow(sample.X / ref.X, (1.0 / 3.0)) - qPow(sample.Y / ref.Y, (1.0 / 3.0)));
    lab.b = 200 * (qPow(sample.Y / ref.Y, (1.0/3.0)) - qPow(sample.Z / ref.Z, (1.0 / 3.0)));

    return lab;
}

void Cqs::countDeltaEsat()
{
    for (int i = 0; i < CQS_SAMPLE_COUNT; i++)
    {
        if (m_deltaC[i] <= 0)
            m_deltaEsat[i] = m_deltaE[i];
        else
            m_deltaEsat[i] = qSqrt(qPow(m_deltaE[i], 2) - qPow(m_deltaC[i], 2));
    }
}

void Cqs::countDeltaErms()
{
    qreal sum = 0;
    for (int i = 0; i < CQS_SAMPLE_COUNT; i++)
        sum += qPow(m_deltaEsat[i], 2);

    m_deltaErms = qSqrt(sum / 15);
}

void Cqs::countScalFact()
{
    m_qaRms = 100 - 3.1 * m_deltaErms;
    m_qa0_100 = 10 * qLn(qExp(m_qaRms / 10) + 1);
}

void Cqs::countCCTFactor()
{
    qreal temp = m_testChroma.temperature().temp();
    if (temp < 3500)
        m_mCCT = (qPow(temp, 3) * 9.2672e-11) - (qPow(temp, 2) * 8.3959e-7) + (temp * 0.00255) - 1.612;
    else
        m_mCCT = 1;
}

void Cqs::countChromaRatio()
{
    for (int i = 0; i < CQS_SAMPLE_COUNT; i++)
        m_chromaRatio[i] = m_cTest[i] / m_cRef[i] * 100;
}

void Cqs::countQa()
{
    m_qa = m_mCCT * m_qa0_100;
}

void Cqs::countQi()
{
    for (int i = 0; i < CQS_SAMPLE_COUNT; i++)
    {
        qreal qiPRE = 100 - 3.1 * m_deltaEsat[i];
        qreal qi0_100 = 10 * qLn(qExp(qiPRE / 10) + 1);
        m_qi[i] = m_mCCT * qi0_100;
    }
}




