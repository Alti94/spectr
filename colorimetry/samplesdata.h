#ifndef SAMPLES_H
#define SAMPLES_H

#include <QtCore>
#include "colorimetric_structs.h"

class SamplesData
{
public:
    static QVector<qreal> planckianWave(qreal temp);
    static QVector<qreal> daylightWave(qreal temp);

    static const CMF_XYZ cmfXYZ_2[95];
    static const CMF_XYZ cmfXYZ_10[95];

    static const QVector<qreal> samplesTCS[14];
    static const QVector<qreal> samplesVS[15];
    static const QVector<qreal> samplesTM[99];

    static const qreal daylightComponents[95][3]; // S0 S1 S2
private:
    static void normY100(QVector<qreal> &wave);
    SamplesData();


};

#endif // SAMPLES_H
