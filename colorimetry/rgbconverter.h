#ifndef RGBCONVERTER_H
#define RGBCONVERTER_H

#include <QtCore>
#include "colorimetric_structs.h"
#include "cqs.h"

class RgbConverter
{
public:
    RgbConverter(const CIE_XYZ &refXyz);

    QColor XYZtoRgb255(const CIE_XYZ &xyz, bool *outOfGamut = 0);
    static QColor LabToRgb255(const CieLAB &lab);

private:
    CIE_XYZ m_refXyz;
    static const CIE_XYZ m_d65Xyz;
    static const qreal sRgbMat[3][3];

    CIE_XYZ d65adapt(const CIE_XYZ &xyz);
    static qreal compand(qreal linear);
};

#endif // RGBCONVERTER_H
