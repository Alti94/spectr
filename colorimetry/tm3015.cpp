#include "tm3015.h"

#define TM_SAMPLE_COUNT 99
#define TM_BIN_COUNT 16

Tm3015::Tm3015()
{

}

Tm3015::Tm3015(const Chromacity &testChroma) :
    m_testChroma(testChroma),
    m_testSamXYZ(QVector<CIE_XYZ>(TM_SAMPLE_COUNT)),
    m_refSamXYZ(QVector<CIE_XYZ>(TM_SAMPLE_COUNT)),
    m_testSamJab(QVector<Jab>(TM_SAMPLE_COUNT)),
    m_refSamJab(QVector<Jab>(TM_SAMPLE_COUNT)),
    m_colorErr(QVector<qreal>(TM_SAMPLE_COUNT)),
    m_samRf(QVector<qreal>(TM_SAMPLE_COUNT)),

    m_sampleTheta(QVector<qreal>(TM_SAMPLE_COUNT)),
    m_sampleBin(QVector<int>(TM_SAMPLE_COUNT)),
    m_testBinJab(QVector<Jab>(TM_BIN_COUNT, {0, 0, 0})),
    m_refBinJab(QVector<Jab>(TM_BIN_COUNT, {0, 0, 0})),
    m_binColorErr(QVector<qreal>(TM_BIN_COUNT, 0)),
    m_binTheta(QVector<qreal>(TM_BIN_COUNT, 0)),
    m_testBinDist(QVector<QPointF>(TM_BIN_COUNT)),
    m_refBinDist(QVector<QPointF>(TM_BIN_COUNT)),
    m_binRf(QVector<qreal>(TM_BIN_COUNT))
{
    selectReference();
    countSampleChroma();
    countJabs();
    countColorError();
    countRf();

    selectBinSamples();
    createBins();
    countRg();
    countDistortions();
    countBinRf();
}

QVector<qreal> Tm3015::binRf() const
{
    return m_binRf;
}

qreal Tm3015::rf() const
{
    return m_rf;
}

qreal Tm3015::rg() const
{
    return m_rg;
}

QVector<QPointF> Tm3015::testBinDist() const
{
    return m_testBinDist;
}

QVector<QPointF> Tm3015::refBinDist() const
{
    return m_refBinDist;
}

void Tm3015::selectReference()
{
    qreal testTemp = m_testChroma.temperature().temp();

    if (testTemp <= 4500)
        m_referenceChroma = Chromacity(SamplesData::planckianWave(testTemp));
    else if (testTemp >= 5500)
        m_referenceChroma = Chromacity(SamplesData::daylightWave(testTemp));
    else
    {
        QVector<qreal> pWave = SamplesData::planckianWave(testTemp);
        QVector<qreal> dWave = SamplesData::daylightWave(testTemp);
        QVector<qreal> sWave = QVector<qreal>(95);

        qreal pRatio = (5500 - testTemp) / 1000;
        qreal dRatio = 1 - pRatio;

        qreal pYsum = 0;
        qreal dYsum = 0;

        for (int i = 0; i < 95; i++)
        {
            pYsum += pWave[i] * SamplesData::cmfXYZ_2[i].y;
            dYsum += dWave[i] * SamplesData::cmfXYZ_2[i].y;
        }

        for (int i = 0; i < 95; i++)
        {
            qreal p = pWave[i] / pYsum * 100;
            qreal d = dWave[i] / dYsum * 100;
            sWave[i] = (p * pRatio) + (d * dRatio);
        }

        m_referenceChroma = Chromacity(sWave, true);
    }
}

void Tm3015::countSampleChroma()
{
    QVector<qreal> testWave = m_testChroma.yNormWave();
    QVector<qreal> refWave = m_referenceChroma.yNormWave();

    for (int i = 0; i < TM_SAMPLE_COUNT; i++)
    {
        m_testSamXYZ[i] = { (qreal) 0, (qreal) 0, (qreal) 0 };
        m_refSamXYZ[i] = { (qreal) 0, (qreal) 0, (qreal) 0 };

        qreal refYsum = 0;
        qreal testYsum = 0;

        for (int j = 0; j < 95; j++)
        {
            m_testSamXYZ[i].X += testWave[j] * SamplesData::cmfXYZ_10[j].x * SamplesData::samplesTM[i][j];
            m_testSamXYZ[i].Y += testWave[j] * SamplesData::cmfXYZ_10[j].y * SamplesData::samplesTM[i][j];
            m_testSamXYZ[i].Z += testWave[j] * SamplesData::cmfXYZ_10[j].z * SamplesData::samplesTM[i][j];
            testYsum += testWave[j] * SamplesData::cmfXYZ_10[j].y;

            m_refSamXYZ[i].X += refWave[j] * SamplesData::cmfXYZ_10[j].x * SamplesData::samplesTM[i][j];
            m_refSamXYZ[i].Y += refWave[j] * SamplesData::cmfXYZ_10[j].y * SamplesData::samplesTM[i][j];
            m_refSamXYZ[i].Z += refWave[j] * SamplesData::cmfXYZ_10[j].z * SamplesData::samplesTM[i][j];
            refYsum += refWave[j] * SamplesData::cmfXYZ_10[j].y;
        }

        m_testSamXYZ[i].X = 100 * m_testSamXYZ[i].X / testYsum;
        m_testSamXYZ[i].Y = 100 * m_testSamXYZ[i].Y / testYsum;
        m_testSamXYZ[i].Z = 100 * m_testSamXYZ[i].Z / testYsum;

        m_refSamXYZ[i].X = 100 * m_refSamXYZ[i].X / refYsum;
        m_refSamXYZ[i].Y = 100 * m_refSamXYZ[i].Y / refYsum;
        m_refSamXYZ[i].Z = 100 * m_refSamXYZ[i].Z / refYsum;
    }

    m_testXYZ = { (qreal) 0, (qreal) 0, (qreal) 0 };
    m_refXYZ = { (qreal) 0, (qreal) 0, (qreal) 0 };

    for (int j = 0; j < 95; j++)
    {
        m_testXYZ.X += testWave[j] * SamplesData::cmfXYZ_10[j].x;
        m_testXYZ.Y += testWave[j] * SamplesData::cmfXYZ_10[j].y;
        m_testXYZ.Z += testWave[j] * SamplesData::cmfXYZ_10[j].z;

        m_refXYZ.X += refWave[j] * SamplesData::cmfXYZ_10[j].x;
        m_refXYZ.Y += refWave[j] * SamplesData::cmfXYZ_10[j].y;
        m_refXYZ.Z += refWave[j] * SamplesData::cmfXYZ_10[j].z;
    }

    m_testXYZ.X = 100 * m_testXYZ.X / m_testXYZ.Y;
    m_testXYZ.Z = 100 * m_testXYZ.Z / m_testXYZ.Y;
    m_testXYZ.Y = 100;

    m_refXYZ.X = 100 * m_refXYZ.X / m_refXYZ.Y;
    m_refXYZ.Z = 100 * m_refXYZ.Z / m_refXYZ.Y;
    m_refXYZ.Y = 100;
}

void Tm3015::countJabs()
{
    m_testJab = toJab(m_testXYZ, m_testXYZ);
    m_refJab = toJab(m_refXYZ, m_refXYZ);

    for (int i = 0; i < TM_SAMPLE_COUNT; i++)
    {
        m_testSamJab[i] = toJab(m_testXYZ, m_testSamXYZ[i]);
        m_refSamJab[i] = toJab(m_refXYZ, m_refSamXYZ[i]);
    }
}

Jab Tm3015::toJab(const CIE_XYZ &ref, const CIE_XYZ &sample)
{
    const qreal LA = 100;
    const qreal Yb = 20;
    const qreal c = 0.69;
    const qreal Nc = 1;

    qreal k = 1 / (5 * LA + 1);
    qreal FL = 0.2 * qPow(k, 4) * 5 * LA + 0.1 * qPow(1 -  qPow(k, 4), 2) * qPow(5 * LA, (1.0 / 3.0));
    qreal n = Yb / ref.Y;
    qreal Nbb = 0.725 * qPow(1 / n, 0.2);
    qreal Ncb = Nbb;
    qreal z = 1.48 + qPow(n, 0.5);

    QVector<qreal> RGB = matMul33x31(cat02, QVector<qreal>({ sample.X, sample.Y, sample.Z }));
    QVector<qreal> RwGwBw = matMul33x31(cat02, QVector<qreal>({ ref.X, ref.Y, ref.Z }));

    qreal Rc = (ref.Y / RwGwBw[0]) * RGB[0];
    qreal Gc = (ref.Y / RwGwBw[1]) * RGB[1];
    qreal Bc = (ref.Y / RwGwBw[2]) * RGB[2];
    qreal Rcw = (ref.Y / RwGwBw[0]) * RwGwBw[0];
    qreal Gcw = (ref.Y / RwGwBw[1]) * RwGwBw[1];
    qreal Bcw = (ref.Y / RwGwBw[2]) * RwGwBw[2];

    QVector<qreal> XcYcZc = matMul33x31(invCat02, QVector<qreal>({ Rc, Gc, Bc }));
    QVector<qreal> XcwYcwZcw = matMul33x31(invCat02, QVector<qreal>({ Rcw, Gcw, Bcw }));
    QVector<qreal> RpGpBp = matMul33x31(mhpe, XcYcZc);
    QVector<qreal> RpwGpwBpw = matMul33x31(mhpe, XcwYcwZcw);

    qreal Rpa = ((400 * qPow(FL * RpGpBp[0] / 100, 0.42)) / ((qPow(FL * RpGpBp[0] / 100, 0.42)) + 27.13)) + 0.1;
    qreal Gpa = ((400 * qPow(FL * RpGpBp[1] / 100, 0.42)) / ((qPow(FL * RpGpBp[1] / 100, 0.42)) + 27.13)) + 0.1;
    qreal Bpa = ((400 * qPow(FL * RpGpBp[2] / 100, 0.42)) / ((qPow(FL * RpGpBp[2] / 100, 0.42)) + 27.13)) + 0.1;

    qreal Rpaw = ((400 * qPow(FL * RpwGpwBpw[0] / 100, 0.42)) / ((qPow(FL * RpwGpwBpw[0] / 100, 0.42)) + 27.13)) + 0.1;
    qreal Gpaw = ((400 * qPow(FL * RpwGpwBpw[1] / 100, 0.42)) / ((qPow(FL * RpwGpwBpw[1] / 100, 0.42)) + 27.13)) + 0.1;
    qreal Bpaw = ((400 * qPow(FL * RpwGpwBpw[2] / 100, 0.42)) / ((qPow(FL * RpwGpwBpw[2] / 100, 0.42)) + 27.13)) + 0.1;

    qreal a = Rpa - 12 * Gpa / 11 + Bpa / 11;
    qreal b = (1.0 / 9.0) * (Rpa + Gpa - 2 * Bpa);

    qreal h;
    if (a == 0) {
        if (b == 0) {
            h = 0;
        }
        else {
            if (b >= 0)
                h = (360 / (2 * M_PI)) * qAtan2(b, a);
            else
                h = 360 + (360 / (2 * M_PI)) * qAtan2(b, a);
        }
    }
    else {
        if (b >= 0)
            h = (360 / (2 * M_PI)) * qAtan2(b, a);
        else
            h = 360 + (360 / (2 * M_PI)) * qAtan2(b, a);
    }

    qreal e = ((12500 / 13) * Nc * Ncb) * (qCos((h * M_PI / 180) + 2) + 3.8);
    qreal A = (2 * Rpa + Gpa + (1.0 / 20.0) * Bpa - 0.305) * Nbb;
    qreal Aw = (2 * Rpaw + Gpaw + (1.0 / 20.0) * Bpaw - 0.305) * Nbb;
    qreal J = 100 * qPow(A / Aw, c * z);
    qreal t = (e * qPow(qPow(a, 2) + qPow(b, 2), 0.5)) / (Rpa + Gpa + (21.0 / 20.0) * Bpa);

    qreal C = qPow(t, 0.9) * qPow(J / 100, 0.5) * qPow(1.64 - qPow(0.29, n), 0.73);
    qreal M = C * qPow(FL, 0.25);

    Jab out;
    out.J = (1 + 100 * 0.007) * J / (1 + 0.007 * J);
    qreal Mc = (1 / 0.0228) * qLn(1 + 0.0228 * M);
    out.a = Mc * qCos(h * M_PI / 180);
    out.b = Mc * qSin(h * M_PI / 180);

    return out;
}

void Tm3015::countColorError()
{
    m_avgColorErr = 0;

    for (int i = 0; i < TM_SAMPLE_COUNT; i++)
    {
        m_colorErr[i] = qSqrt(qPow(m_refSamJab[i].J - m_testSamJab[i].J, 2) +
                              qPow(m_refSamJab[i].a - m_testSamJab[i].a, 2) +
                              qPow(m_refSamJab[i].b - m_testSamJab[i].b, 2));

        m_avgColorErr += m_colorErr[i];
    }

    m_avgColorErr /= TM_SAMPLE_COUNT;
}

void Tm3015::countRf()
{
    const qreal cfactor = 7.54;

    for (int i = 0; i < TM_SAMPLE_COUNT; i++)
        m_samRf[i] = 10 * qLn(qExp((100 - cfactor * m_colorErr[i]) / 10) + 1);

    m_rf = 10 * qLn(qExp((100 - cfactor * m_avgColorErr) / 10) + 1);
}

void Tm3015::selectBinSamples()
{
    for (int i = 0; i < TM_SAMPLE_COUNT; i++)
    {
        int tmp1 = (m_refSamJab[i].a < 0 && m_refSamJab[i].b > 0) ? 1 : 0;
        int tmp2 = (m_refSamJab[i].a < 0 && m_refSamJab[i].b < 0) ? 1 : 0;
        int tmp3 = (m_refSamJab[i].a > 0 && m_refSamJab[i].b < 0) ? 1 : 0;
        m_sampleTheta[i] = qAtan(m_refSamJab[i].b / m_refSamJab[i].a) + M_PI * tmp1 + M_PI * tmp2 + 2 * M_PI * tmp3;

        m_sampleBin[i] = qFloor(((m_sampleTheta[i] / 2) / M_PI) * 16);
    }
}

void Tm3015::createBins()
{
    QVector<int> binCount = QVector<int>(TM_BIN_COUNT, 0);

    for (int i = 0; i < TM_SAMPLE_COUNT; i++)
    {
        int binNum = m_sampleBin[i];
        m_testBinJab[binNum].J += m_testSamJab[i].J;
        m_testBinJab[binNum].a += m_testSamJab[i].a;
        m_testBinJab[binNum].b += m_testSamJab[i].b;
        m_refBinJab[binNum].J += m_refSamJab[i].J;
        m_refBinJab[binNum].a += m_refSamJab[i].a;
        m_refBinJab[binNum].b += m_refSamJab[i].b;
        m_binColorErr[binNum] += m_colorErr[i];
        m_binTheta[binNum] += m_sampleTheta[i];

        binCount[binNum]++;
    }

    for (int i = 0; i < TM_BIN_COUNT; i++)
    {
        m_testBinJab[i].J /= binCount[i];
        m_testBinJab[i].a /= binCount[i];
        m_testBinJab[i].b /= binCount[i];
        m_refBinJab[i].J /= binCount[i];
        m_refBinJab[i].a /= binCount[i];
        m_refBinJab[i].b /= binCount[i];
        m_binColorErr[i] /= binCount[i];
        m_binTheta[i] /= binCount[i];
    }
}

void Tm3015::countRg()
{
    QVector<qreal> diffAr = QVector<qreal>(TM_BIN_COUNT);
    QVector<qreal> diffA = QVector<qreal>(TM_BIN_COUNT);
    QVector<qreal> meanBr = QVector<qreal>(TM_BIN_COUNT);
    QVector<qreal> meanB = QVector<qreal>(TM_BIN_COUNT);

    for (int i = 0; i < TM_BIN_COUNT - 1; i++)
    {
        diffAr[i] = m_refBinJab[i + 1].a - m_refBinJab[i].a;
        diffA[i] = m_testBinJab[i + 1].a - m_testBinJab[i].a;
        meanBr[i] = (m_refBinJab[i + 1].b + m_refBinJab[i].b) / 2;
        meanB[i] = (m_testBinJab[i + 1].b + m_testBinJab[i].b) / 2;
    }

    diffAr[TM_BIN_COUNT - 1] = m_refBinJab[0].a - m_refBinJab[TM_BIN_COUNT - 1].a;
    diffA[TM_BIN_COUNT - 1] = m_testBinJab[0].a - m_testBinJab[TM_BIN_COUNT - 1].a;
    meanBr[TM_BIN_COUNT - 1] = (m_refBinJab[0].b + m_refBinJab[TM_BIN_COUNT - 1].b) / 2;
    meanB[TM_BIN_COUNT - 1] = (m_testBinJab[0].b + m_testBinJab[TM_BIN_COUNT - 1].b) / 2;

    qreal A0 = 0;
    qreal A1 = 0;

    for (int i = 0; i < TM_BIN_COUNT; i++)
    {
        A0 += diffAr[i] * meanBr[i];
        A1 += diffA[i] * meanB[i];
    }

    m_rg = A1 / A0 * 100;
}

void Tm3015::countDistortions()
{
    for (int i = 0; i < TM_BIN_COUNT; i++)
    {
        qreal daRel = (m_testBinJab[i].a - m_refBinJab[i].a) / qSqrt(qPow(m_refBinJab[i].a, 2) + qPow(m_refBinJab[i].b, 2));
        qreal dbRel = (m_testBinJab[i].b - m_refBinJab[i].b) / qSqrt(qPow(m_refBinJab[i].a, 2) + qPow(m_refBinJab[i].b, 2));
        QPointF rel(daRel, dbRel);


        m_refBinDist[i] = QPointF(qCos(m_binTheta[i]), qSin(m_binTheta[i]));
        m_testBinDist[i] = m_refBinDist[i] + rel;
    }
}

void Tm3015::countBinRf()
{
    const qreal cfactor = 7.54;
    for (int i = 0; i < TM_BIN_COUNT; i++)
    {
        m_binRf[i] = 10 * qLn(qExp((100 - cfactor * m_binColorErr[i]) / 10) + 1);
    }
}

const QVector<QVector<qreal>> Tm3015::cat02 = QVector<QVector<qreal>>({
                                                                          QVector<qreal>({ 0.7328, 0.4296, -0.1624 }),
                                                                          QVector<qreal>({ -0.7036, 1.6975, 0.0061 }),
                                                                          QVector<qreal>({ 0.0030, 0.0136, 0.9834 }),
                                                                      });

const QVector<QVector<qreal>> Tm3015::invCat02 = QVector<QVector<qreal>>({
                                                                             QVector<qreal>({ 1.09612382083551, -0.278869000218287, 0.182745179382773 }),
                                                                             QVector<qreal>({ 0.454369041975359, 0.473533154307412, 0.0720978037172291 }),
                                                                             QVector<qreal>({ -0.00962760873842936, -0.00569803121611342, 1.01532563995454}),
                                                                         });

const QVector<QVector<qreal>> Tm3015::mhpe = QVector<QVector<qreal>>({
                                                                         QVector<qreal>({ 0.389710, 0.688980, -0.078680 }),
                                                                         QVector<qreal>({ -0.229810, 1.183400, 0.046410 }),
                                                                         QVector<qreal>({ 0.000000, 0.000000, 1.000000 }),
                                                                     });

const QVector<QVector<qreal>> Tm3015::invMhpe = QVector<QVector<qreal>>({
                                                                            QVector<qreal>({ 1.91019683405203, -1.11212389278787, 0.201907956767499 }),
                                                                            QVector<qreal>({ 0.370950088248689, 0.629054257392613, -8.05514218436133e-06 }),
                                                                            QVector<qreal>({ 0, 0, 1 }),
                                                                        });

QVector<qreal> Tm3015::matMul33x31(const QVector<QVector<qreal>> &mat33, const QVector<qreal> &mat31)
{
    QVector<qreal> out = QVector<qreal>(3);
    out[0] = mat33[0][0] * mat31[0] + mat33[0][1] * mat31[1] + mat33[0][2] * mat31[2];
    out[1] = mat33[1][0] * mat31[0] + mat33[1][1] * mat31[1] + mat33[1][2] * mat31[2];
    out[2] = mat33[2][0] * mat31[0] + mat33[2][1] * mat31[1] + mat33[2][2] * mat31[2];

    return out;
}
