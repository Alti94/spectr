#ifndef CHROMATICADAPT_H
#define CHROMATICADAPT_H

#include "colorimetric_structs.h"

class ChromaticAdapt
{
public:
    static CIE_XYZ cmccat2000Adapt(const CIE_XYZ sample, const CIE_XYZ test, const CIE_XYZ reference, qreal lumTest, qreal lumReference);
    static CIE_XYZ vonKriesAdapt(const CIE_XYZ sample, const CIE_XYZ test, const CIE_XYZ reference);

private:
    static const QVector<QVector<qreal>> cmccat2000Mat;
    static const QVector<QVector<qreal>> cmccat2000InvMat;

    static const QVector<QVector<qreal>> juddMat;
    static const QVector<QVector<qreal>> juddInvMat;

    ChromaticAdapt();
    static QVector<qreal> matMul33x31(const QVector<QVector<qreal>> &mat33, const QVector<qreal> &mat31);


};

#endif // CHROMATICADAPT_H
