#include "qubicspline.h"

QubicSpline::QubicSpline(const QVector<QPointF> &data)
{
    this->m_data = data;
}

QubicSpline::QubicSpline(const QubicSpline &obj)
{
    this->m_data = obj.m_data;
}

QVector<QPointF> QubicSpline::spline(qreal startX, qreal endX, qreal period)
{
    QVector<qreal> points;

    for (qreal i = startX; i <= endX; i += period)
        points.append(i);

    return spline(points);
}

QVector<QPointF> QubicSpline::spline(const QVector<qreal> &points)
{
    QVector<QPointF> newData;
    if (m_data.length() < 3)
        return m_data;

    QVector<Row> helper;

    for (int i = 0; i < m_data.length(); i++)
    {
        qreal actLen = m_data[i].x();
        qreal nextLen = ((i + 1) < m_data.length()) ? m_data[i + 1].x() : 0;
        Row row = {
            m_data[i].x(),
            m_data[i].y(),
            nextLen - actLen,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        };

        if (i > 0)
        {
            row.y1pp = 2 * (row.hi + helper[i - 1].hi);
            qreal nextD = ((i + 1) < m_data.length()) ? m_data[i + 1].y() : 0;
            row.one = (6 * (row.d - helper[i - 1].d) / helper[i - 1].hi) - (6 * (nextD - row.d) / row.hi);
            qreal a = (6 * (row.d - helper[i - 1].d) / helper[i - 1].hi);
            qreal b = (6 * (nextD - row.d) / row.hi);
            qreal c = a - b;
            row.m = row.y1pp - helper[i - 1].hi * helper[i - 1].l;
            row.l = row.hi / row.m;
            row.z = row.one - helper[i - 1].l * helper[i - 1].z;
        }

        helper.append(row);
    }

    {
        Row row = helper.last();
        row.ypp = -row.z / row.m;
        row.c = -row.d / row.hi;
        helper.replace(helper.length() - 1, row);
    }

    for (int i = helper.length() - 2; i >= 1; i--)
    {
        Row row = helper[i];

        row.ypp = -(row.z + (row.hi * helper[i + 1].ypp)) / row.m;
        row.a = (helper[i + 1].ypp - row.ypp) / 6 / row.hi;
        row.b = row.ypp / 2;
        row.c = (helper[i + 1].d - row.d) / row.hi - (helper[i + 1].ypp + 2 * row.ypp) / 6 * row.hi;

        helper.replace(i, row);
    }

    {
        Row row = helper.first();
        row.ypp = 0;
        row.a = (helper[1].ypp - row.ypp) / 6 / row.hi;
        row.b = row.ypp / 2;
        row.c = (helper[1].d - row.d) / row.hi - (helper[1].ypp + 2 * row.ypp) / 6 * row.hi;
        helper.replace(0, row);
    }

    int x = 0;
    int fillBefore = 0, fillAfter = 0;
    for (int j = 0; j < points.length(); j++)
    {
        qreal i = points[j];

        if (i < helper.first().wl)
        {
            fillBefore++;
            continue;
        }

        if (i > helper.last().wl)
        {
            fillAfter++;
            continue;
        }

        while (i > helper[x + 1].wl) {
            x++;
        }

        qreal wl = i - helper[x].wl;
        qreal amp = helper[x].a * qPow(wl, 3) + helper[x].b * qPow(wl, 2) + helper[x].c * wl + helper[x].d;
        if (amp < 0) amp = 0;

        QPointF point(i, amp);
        newData.append(point);
    }

    QPointF point = newData.first();
    for (; fillBefore > 0; fillBefore--)
        newData.push_front(QPointF(points[fillBefore - 1], point.y()));

    point = newData.last();
    for (; fillAfter > 0; fillAfter--)
        newData.push_back(QPointF(points[points.size() - fillAfter], point.y()));

    return newData;
}
