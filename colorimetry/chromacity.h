#ifndef CHROMACITY_H
#define CHROMACITY_H

#include "samplesdata.h"
#include "temperature.h"

class Chromacity
{
public:
    Chromacity();
    Chromacity(const QVector<QPointF> &wave);
    Chromacity(const QVector<qreal> &wave, bool yNormalized = false);
    Chromacity(const Chromacity &obj);

    QVector<qreal> yNormWave() const;
    CIE_XYZ xyz() const;
    XYZ_CHROMA xyzChroma() const;
    UV60_CHROMA uv60Chroma() const;
    UV76_CHROMA uv76Chroma() const;
    Temperature temperature() const;

    static XYZ_CHROMA toXyzChroma(const CIE_XYZ cieXYZ);
    static UV60_CHROMA toUv60Chroma(const CIE_XYZ cieXYZ);
    static UV76_CHROMA toUv76Chroma(const CIE_XYZ cieXYZ);

private:
    QVector<qreal> m_testWave;
    CIE_XYZ m_xyz;
    XYZ_CHROMA m_xyzChroma;
    UV60_CHROMA m_uv60Chroma;
    UV76_CHROMA m_uv76Chroma;
    Temperature m_temperature;

    void init();
    void normY100(const QVector<QPointF> &wave);
    void normY100(const QVector<qreal> &wave);
    void countCIE_XYZ_1931();
    void countXyzChroma();
    void countUv60Chroma();
    void countUv76Chroma();

};

#endif // CHROMACITY_H
