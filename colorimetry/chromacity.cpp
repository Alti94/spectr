#include "chromacity.h"
#include "qubicspline.h"
#include "samplesdata.h"

Chromacity::Chromacity()
{
}

Chromacity::Chromacity(const QVector<QPointF> &wave)
{
    QubicSpline spline(wave);
    normY100(spline.spline(360, 830, 5));
    init();
}

Chromacity::Chromacity(const QVector<qreal> &wave, bool yNormalized)
{
    if (yNormalized)
        m_testWave = wave;
    else
        normY100(wave);
    init();
}

Chromacity::Chromacity(const Chromacity &obj) :
    m_testWave(obj.m_testWave),
    m_xyz(obj.m_xyz),
    m_xyzChroma(obj.m_xyzChroma),
    m_uv60Chroma(obj.m_uv60Chroma),
    m_uv76Chroma(obj.m_uv76Chroma),
    m_temperature(obj.m_temperature)
{
}

QVector<qreal> Chromacity::yNormWave() const
{
    return m_testWave;
}

CIE_XYZ Chromacity::xyz() const
{
    return m_xyz;
}

XYZ_CHROMA Chromacity::xyzChroma() const
{
    return m_xyzChroma;
}

UV60_CHROMA Chromacity::uv60Chroma() const
{
    return m_uv60Chroma;
}

UV76_CHROMA Chromacity::uv76Chroma() const
{
    return m_uv76Chroma;
}

Temperature Chromacity::temperature() const
{
    return m_temperature;
}

void Chromacity::init()
{
    countCIE_XYZ_1931();
    countXyzChroma();
    countUv60Chroma();
    countUv76Chroma();

    m_temperature = Temperature(m_uv60Chroma);
}

void Chromacity::countCIE_XYZ_1931()
{
    m_xyz.X = 0;
    m_xyz.Y = 0;
    m_xyz.Z = 0;

    for (int i = 0; i < 95; i++)
    {
        m_xyz.X += m_testWave[i] * SamplesData::cmfXYZ_2[i].x;
        m_xyz.Y += m_testWave[i] * SamplesData::cmfXYZ_2[i].y;
        m_xyz.Z += m_testWave[i] * SamplesData::cmfXYZ_2[i].z;
    }
}

void Chromacity::countXyzChroma()
{
    qreal sum = m_xyz.X + m_xyz.Y + m_xyz.Z;
    m_xyzChroma.x = m_xyz.X / sum;
    m_xyzChroma.y = m_xyz.Y / sum;
    m_xyzChroma.z = m_xyz.Z / sum;
}

void Chromacity::countUv60Chroma()
{
    qreal sum = m_xyz.X + (15 * m_xyz.Y) + (3 * m_xyz.Z);
    m_uv60Chroma.u = (4 * m_xyz.X) / sum;
    m_uv60Chroma.v = (6 * m_xyz.Y) / sum;
}

void Chromacity::countUv76Chroma()
{
    qreal sum = m_xyz.X + (15 * m_xyz.Y) + (3 * m_xyz.Z);
    m_uv76Chroma.uP = (4 * m_xyz.X) / sum;
    m_uv76Chroma.vP = (9 * m_xyz.Y) / sum;
}


void Chromacity::normY100(const QVector<QPointF> &wave)
{
    qreal sum = 0;

    for (int i = 0; i < 95; i++)
        sum += wave[i].y() * SamplesData::cmfXYZ_2[i].y;

    for (int i = 0; i < 95; i++)
        m_testWave << wave[i].y() / sum * 100;
}

void Chromacity::normY100(const QVector<qreal> &wave)
{
    qreal sum = 0;

    for (int i = 0; i < 95; i++)
        sum += wave[i] * SamplesData::cmfXYZ_2[i].y;

    for (int i = 0; i < 95; i++)
        m_testWave << wave[i] / sum * 100;
}

XYZ_CHROMA Chromacity::toXyzChroma(const CIE_XYZ cieXYZ)
{
    qreal sum = cieXYZ.X + cieXYZ.Y + cieXYZ.Z;
    XYZ_CHROMA out;
    out.x = cieXYZ.X / sum;
    out.y = cieXYZ.Y / sum;
    out.z = cieXYZ.Z / sum;

    return out;
}

UV60_CHROMA Chromacity::toUv60Chroma(const CIE_XYZ cieXYZ)
{
    qreal sum = cieXYZ.X + (15 * cieXYZ.Y) + (3 * cieXYZ.Z);
    UV60_CHROMA out;
    out.u = (4 * cieXYZ.X) / sum;
    out.v = (6 * cieXYZ.Y) / sum;

    return out;
}

UV76_CHROMA Chromacity::toUv76Chroma(const CIE_XYZ cieXYZ)
{
    qreal sum = cieXYZ.X + (15 * cieXYZ.Y) + (3 * cieXYZ.Z);
    UV76_CHROMA out;
    out.uP = (4 * cieXYZ.X) / sum;
    out.vP = (9 * cieXYZ.Y) / sum;

    return out;
}
