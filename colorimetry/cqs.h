#ifndef CQS_H
#define CQS_H

#include <QtCore>
#include "chromacity.h"

class Cqs
{
public:
    Cqs();
    Cqs(const Chromacity &testChroma);
    Cqs(const Cqs &obj);

    Chromacity testChroma() const;
    Chromacity referenceChroma() const;
    QVector<CIE_XYZ> testSamXYZ() const;
    QVector<CIE_XYZ> refSamXYZ() const;
    QVector<qreal> qi() const;
    qreal qa() const;
    QVector<qreal> chromaRatio() const;

private:
    Chromacity m_testChroma;
    Chromacity m_referenceChroma;

    QVector<CIE_XYZ> m_testSamXYZ, m_refSamXYZ;
    QVector<CIE_XYZ> m_testSamAdXYZ;
    CIE_XYZ m_testAdXYZ;

    QVector<CieLAB> m_testSamLabs, m_refSamLabs;
    QVector<qreal> m_cTest, m_cRef;
    QVector<qreal> m_deltaC;
    QVector<qreal> m_deltaE, m_deltaEsat;
    qreal m_deltaErms;
    qreal m_qaRms;
    qreal m_qa0_100;
    qreal m_mCCT;

    QVector<qreal> m_chromaRatio;
    QVector<qreal> m_qi;
    qreal m_qa;

    void selectReference();
    void countSamplesChroma();
    void chromaAdaptTestXyz();
    void countLabs();
    CieLAB toLab(const CIE_XYZ &ref, const CIE_XYZ &sample);
    void countDeltaEsat();
    void countDeltaErms();
    void countScalFact();
    void countCCTFactor();

    void countChromaRatio();
    void countQa();
    void countQi();

    CIE_RGB toRGB(const CIE_XYZ &xyz);
    CIE_XYZ toXYZ(const CIE_RGB &rgb);
};

#endif // CQS_H
