#ifndef TM3015_H
#define TM3015_H

#include <QtCore>
#include "chromacity.h"

class Tm3015
{
public:
    Tm3015();
    Tm3015(const Chromacity &testChroma);

    QVector<qreal> binRf() const;
    qreal rf() const;
    qreal rg() const;

    QVector<QPointF> testBinDist() const;
    QVector<QPointF> refBinDist() const;

private:
    Chromacity m_testChroma;
    Chromacity m_referenceChroma;
    CIE_XYZ m_testXYZ, m_refXYZ;
    QVector<CIE_XYZ> m_testSamXYZ, m_refSamXYZ;
    Jab m_testJab, m_refJab;
    QVector<Jab> m_testSamJab, m_refSamJab;
    QVector<qreal> m_colorErr;
    qreal m_avgColorErr;
    QVector<qreal> m_samRf;
    qreal m_rf;
    QVector<qreal> m_sampleTheta;
    QVector<int> m_sampleBin;
    QVector<Jab> m_testBinJab, m_refBinJab;
    QVector<qreal> m_binColorErr;
    QVector<qreal> m_binTheta;
    qreal m_rg;
    QVector<QPointF> m_testBinDist;
    QVector<QPointF> m_refBinDist;
    QVector<qreal> m_binRf;




    void selectReference();
    void countSampleChroma();
    void countJabs();
    Jab toJab(const CIE_XYZ &ref, const CIE_XYZ &sample);
    void countColorError();
    void countRf();
    void selectBinSamples();
    void createBins();
    void countRg();
    void countDistortions();
    void countBinRf();

    static const QVector<QVector<qreal>> cat02;
    static const QVector<QVector<qreal>> invCat02;
    static const QVector<QVector<qreal>> mhpe;
    static const QVector<QVector<qreal>> invMhpe;

    static QVector<qreal> matMul33x31(const QVector<QVector<qreal>> &mat33, const QVector<qreal> &mat31);
};

#endif // TM3015_H
