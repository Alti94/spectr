#include "chromaticadapt.h"
#include "math.h"

CIE_XYZ ChromaticAdapt::cmccat2000Adapt(const CIE_XYZ sample, const CIE_XYZ test, const CIE_XYZ reference, qreal lumTest, qreal lumReference)
{
    QVector<qreal> sampleRGB, testRGB, referenceRGB;
    sampleRGB = matMul33x31(cmccat2000Mat, QVector<qreal>({sample.X / sample.Y, sample.Y / sample.Y, sample.Z / sample.Y}));
    testRGB = matMul33x31(cmccat2000Mat, QVector<qreal>({test.X / test.Y, test.Y / test.Y, test.Z / test.Y}));
    referenceRGB = matMul33x31(cmccat2000Mat, QVector<qreal>({reference.X / reference.Y, reference.Y / reference.Y, reference.Z / reference.Y}));

    qreal d = 0.08 * log10(lumTest + lumReference) + 0.76 - 0.45 * (lumTest - lumReference) / (lumTest + lumReference);

    if (d > 1)
        d = 1;
    if (d < 0)
        d = 0;

    QVector<qreal> adaptedRGB = QVector<qreal>(3);

    for (int i = 0; i < 3; i++)
    {
        adaptedRGB[i] = (d * (referenceRGB[i] / testRGB[i] + 1 - d)) * sampleRGB[i];
        adaptedRGB[i] *= sample.Y;
    }

    QVector<qreal> adaptedXYZ = matMul33x31(cmccat2000InvMat, adaptedRGB);

    return { adaptedXYZ[0], adaptedXYZ[1], adaptedXYZ[2] };
}

CIE_XYZ ChromaticAdapt::vonKriesAdapt(const CIE_XYZ sample, const CIE_XYZ test, const CIE_XYZ reference)
{
    QVector<qreal> sampleRGB, testRGB, referenceRGB;
    sampleRGB = matMul33x31(juddMat, QVector<qreal>({ sample.X, sample.Y, sample.Z }));
    testRGB = matMul33x31(juddMat, QVector<qreal>({ test.X, test.Y, test.Z }));
    referenceRGB = matMul33x31(juddMat, QVector<qreal>({ reference.X, reference.Y, reference.Z }));

    qreal alpha, beta, gamma;
    alpha = testRGB[0] / referenceRGB[0];
    beta = testRGB[1] / referenceRGB[1];
    gamma = testRGB[2] / referenceRGB[2];

    QVector<qreal> adaptedRGB = QVector<qreal>(3);

    adaptedRGB[0] = alpha * sampleRGB[0];
    adaptedRGB[1] = beta * sampleRGB[1];
    adaptedRGB[2] = gamma * sampleRGB[2];

    QVector<qreal> adaptedXYZ = matMul33x31(juddInvMat, adaptedRGB);

    return { adaptedXYZ[0], adaptedXYZ[1], adaptedXYZ[2] };
}



ChromaticAdapt::ChromaticAdapt()
{
}

QVector<qreal> ChromaticAdapt::matMul33x31(const QVector<QVector<qreal>> &mat33, const QVector<qreal> &mat31)
{
    QVector<qreal> out = QVector<qreal>(3);
    out[0] = mat33[0][0] * mat31[0] + mat33[0][1] * mat31[1] + mat33[0][2] * mat31[2];
    out[1] = mat33[1][0] * mat31[0] + mat33[1][1] * mat31[1] + mat33[1][2] * mat31[2];
    out[2] = mat33[2][0] * mat31[0] + mat33[2][1] * mat31[1] + mat33[2][2] * mat31[2];

    return out;
}


const QVector<QVector<qreal>> ChromaticAdapt::cmccat2000Mat = QVector<QVector<qreal>>({
                                                                                                QVector<qreal>({ 0.7982, 0.3389, -0.1371 }),
                                                                                                QVector<qreal>({ -0.5918, 1.5512, 0.0406 }),
                                                                                                QVector<qreal>({ 0.0008, 0.0239, 0.9753 }),
                                                                                            });

const QVector<QVector<qreal>> ChromaticAdapt::cmccat2000InvMat = QVector<QVector<qreal>>({
                                                                                                QVector<qreal>({ 1.076450049, -0.237662388, 0.161212339 }),
                                                                                                QVector<qreal>({ 0.410964325, 0.554341804, 0.03469387 }),
                                                                                                QVector<qreal>({ -0.010953765, -0.013389356, 1.024343122 }),
                                                                                            });

const QVector<QVector<qreal>> ChromaticAdapt::juddMat = QVector<QVector<qreal>>({
                                                                                    QVector<qreal>({ 0, 1, 0 }),
                                                                                    QVector<qreal>({ -0.46, 1.36, 0.1 }),
                                                                                    QVector<qreal>({ 0, 0, 1 }),
                                                                                });

const QVector<QVector<qreal>> ChromaticAdapt::juddInvMat = QVector<QVector<qreal>>({
                                                                                       QVector<qreal>({ 2.954, -2.174, 0.22 }),
                                                                                       QVector<qreal>({ 1, 0, 0 }),
                                                                                       QVector<qreal>({ 0, 0, 1 }),
                                                                                   });
