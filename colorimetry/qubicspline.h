#ifndef QUBICSPLINE_H
#define QUBICSPLINE_H

#include <QtCore>

class QubicSpline
{
public:
    struct Row {
        qreal wl;
        qreal d;
        qreal hi;
        qreal y1pp;
        qreal one;
        qreal m;
        qreal l;
        qreal z;
        qreal ypp;
        qreal a;
        qreal b;
        qreal c;
    };

    QubicSpline(const QVector<QPointF> &m_data);
    QubicSpline(const QubicSpline &obj);

    QVector<QPointF> spline(qreal startX, qreal endX, qreal period);
    QVector<QPointF> spline(const QVector<qreal> &points);

private:
    QVector<QPointF> m_data;

};

#endif // QUBICSPLINE_H
