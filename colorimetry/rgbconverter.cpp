#include "rgbconverter.h"
#include "chromaticadapt.h"
#include <QColor>

RgbConverter::RgbConverter(const CIE_XYZ &refXyz) :
    m_refXyz(refXyz)
{
}

QColor RgbConverter::XYZtoRgb255(const CIE_XYZ &xyz, bool *outOfGamut)
{
    // Adaptacja chromatyczna CMCCAT2000 do D65
    CIE_XYZ adapted = ChromaticAdapt::cmccat2000Adapt(xyz, m_refXyz, m_d65Xyz, 1000, 200);

    adapted.X /= m_d65Xyz.Y;
    adapted.Y /= m_d65Xyz.Y;
    adapted.Z /= m_d65Xyz.Y;


    // Konwersja do przestrzeni sRGB
    CIE_RGB rgb;
    rgb.R = adapted.X * sRgbMat[0][0] + adapted.Y * sRgbMat[0][1] + adapted.Z * sRgbMat[0][2];;
    rgb.G = adapted.X * sRgbMat[1][0] + adapted.Y * sRgbMat[1][1] + adapted.Z * sRgbMat[1][2];
    rgb.B = adapted.X * sRgbMat[2][0] + adapted.Y * sRgbMat[2][1] + adapted.Z * sRgbMat[2][2];


    // Sprawdzanie czy poza przestrzenią
    bool outGamut = false;

    if (rgb.R < 0)
    {
        rgb.R = 0;
        outGamut = true;
    }
    if (rgb.G < 0)
    {
        rgb.G = 0;
        outGamut = true;
    }
    if (rgb.B < 0)
    {
        rgb.B = 0;
        outGamut = true;
    }

    if (outOfGamut != 0)
        *outOfGamut = outGamut;

    // Korekcja Gamma
    const qreal gamma = 2.2;

    rgb.R = qPow(rgb.R, 1 / gamma);
    rgb.G = qPow(rgb.G, 1 / gamma);
    rgb.B = qPow(rgb.B, 1 / gamma);

    if (rgb.R > 1)
        rgb.R = 1;
    if (rgb.G > 1)
        rgb.G = 1;
    if (rgb.B > 1)
        rgb.B = 1;

    return QColor::fromRgbF(rgb.R, rgb.G, rgb.B);
}

QColor RgbConverter::LabToRgb255(const CieLAB &lab)
{
    // Lab do XYZ w D65 white
    qreal rX = m_d65Xyz.X / m_d65Xyz.Y;
    qreal rY = m_d65Xyz.Y / m_d65Xyz.Y;
    qreal rZ = m_d65Xyz.Z / m_d65Xyz.Y;

    const qreal kE = 216.0 / 24389.0;
    const qreal kK = 24389.0 / 27.0;
    const qreal kKE = 8;

    qreal fy = (lab.L + 16.0) / 116.0;
    qreal fx = 0.002 * lab.a + fy;
    qreal fz = fy - 0.005 * lab.b;

    qreal fx3 = fx * fx * fx;
    qreal fz3 = fz * fz * fz;

    qreal xr = (fx3 > kE) ? fx3 : ((116.0 * fx - 16.0) / kK);
    qreal yr = (lab.L > kKE) ? qPow((lab.L + 16.0) / 116.0, 3.0) : (lab.L / kK);
    qreal zr = (fz3 > kE) ? fz3 : ((116.0 * fz - 16.0) / kK);

    CIE_XYZ xyz = { xr * rX, yr * rY, zr * rZ };

    // XYZ do sRGB
    CIE_RGB rgb;
    rgb.R = compand(sRgbMat[0][0] * xyz.X + sRgbMat[0][1] * xyz.Y + sRgbMat[0][2] * xyz.Z);
    rgb.G = compand(sRgbMat[1][0] * xyz.X + sRgbMat[1][1] * xyz.Y + sRgbMat[1][2] * xyz.Z);
    rgb.B = compand(sRgbMat[2][0] * xyz.X + sRgbMat[2][1] * xyz.Y + sRgbMat[2][2] * xyz.Z);

    if (rgb.R > 1) rgb.R = 1;
    if (rgb.G > 1) rgb.G = 1;
    if (rgb.B > 1) rgb.B = 1;

    if (rgb.R < 0) rgb.R = 0;
    if (rgb.G < 0) rgb.G = 0;
    if (rgb.B < 0) rgb.B = 0;

    QColor color;
    color.setRgbF(rgb.R, rgb.G, rgb.B);

    return color;
}

qreal RgbConverter::compand(qreal linear)
{
    qreal companded;

    qreal sign = 1.0;
    if (linear < 0.0)
    {
        sign = -1.0;
        linear = -linear;
    }
    companded = (linear <= 0.0031308) ? (linear * 12.92) : (1.055 * qPow(linear, 1.0 / 2.4) - 0.055);
    companded *= sign;

    return companded;
}

const qreal RgbConverter::sRgbMat[3][3] = {
    { 3.2404542, -1.5371385, -0.4985314 },
    { -0.969266, 1.8760108, 0.041556 },
    { 0.0556434, -0.2040259, 1.0572252 },
};

const CIE_XYZ RgbConverter::m_d65Xyz = { 95.048, 100.000, 108.888 };
