#-------------------------------------------------
#
# Project created by QtCreator 2017-12-09T18:17:18
#
#-------------------------------------------------

QT       += core gui charts opengl sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Spectr
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        spectrview.cpp \
    3rdparty/swdll/include/swdll.cpp \
    spectdatalistener.cpp \
    measureview.cpp \
    colorimetry/chromacity.cpp \
    colorimetry/qubicspline.cpp \
    colorimetry/temperature.cpp \
    colorimetry/ciecri.cpp \
    colorimetry/samplesdata.cpp \
    colorimetry/cqs.cpp \
    colorimetry/tm3015.cpp \
    widgets/callout.cpp \
    widgets/spectrumchart.cpp \
    widgets/uv76chart.cpp \
    widgets/proportionalwidget.cpp \
    widgets/crisamplescolors.cpp \
    colorimetry/rgbconverter.cpp \
    colorimetry/chromaticadapt.cpp \
    widgets/barchart.cpp \
    widgets/labchart.cpp \
    widgets/cqssamplescolors.cpp \
    widgets/rgvsrfplot.cpp \
    widgets/items/arrowgraphicsitem.cpp \
    widgets/tmdistortionplot.cpp \
    widgets/cqsicon.cpp \
    csvhandler.cpp \
    widgets/csvfiledialog.cpp \
    widgets/customfiledialog.cpp \
    database/rows/measurerow.cpp \
    database/managers/measuremanager.cpp \
    widgets/savedmeasures.cpp \
    widgets/items/measureitem.cpp \
    widgets/savedialog.cpp \
    widgets/settingsdialog.cpp \
    widgets/calibrationdialog.cpp \
    database/managers/calibrationmanager.cpp \
    widgets/about.cpp

HEADERS += \
        spectrview.h \
    3rdparty/swdll/include/swdll.h \
    spectdatalistener.h \
    measureview.h \
    colorimetry/colorimetric_structs.h \
    colorimetry/chromacity.h \
    colorimetry/qubicspline.h \
    colorimetry/temperature.h \
    colorimetry/ciecri.h \
    colorimetry/samplesdata.h \
    colorimetry/cqs.h \
    colorimetry/tm3015.h \
    widgets/callout.h \
    widgets/spectrumchart.h \
    widgets/uv76chart.h \
    widgets/proportionalwidget.h \
    widgets/crisamplescolors.h \
    colorimetry/rgbconverter.h \
    colorimetry/chromaticadapt.h \
    widgets/barchart.h \
    widgets/labchart.h \
    widgets/cqssamplescolors.h \
    widgets/rgvsrfplot.h \
    widgets/items/arrowgraphicsitem.h \
    widgets/tmdistortionplot.h \
    widgets/cqsicon.h \
    csvhandler.h \
    widgets/csvfiledialog.h \
    widgets/customfiledialog.h \
    database/connection.h \
    database/rows/measurerow.h \
    database/managers/measuremanager.h \
    widgets/savedmeasures.h \
    widgets/items/measureitem.h \
    widgets/savedialog.h \
    widgets/settingsdialog.h \
    widgets/calibrationdialog.h \
    database/managers/calibrationmanager.h \
    widgets/about.h

FORMS += \
        spectrview.ui \
    measureview.ui \
    widgets/proportionalwidget.ui \
    widgets/crisamplescolors.ui \
    widgets/cqssamplescolors.ui \
    widgets/csvfiledialog.ui \
    widgets/savedmeasures.ui \
    widgets/items/measureitem.ui \
    widgets/savedialog.ui \
    widgets/settingsdialog.ui \
    widgets/calibrationdialog.ui \
    widgets/about.ui

INCLUDEPATH += \
        3rdparty/swdll/include

RESOURCES += \
    resources/resources.qrc
