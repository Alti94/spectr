#include "spectrview.h"
#include <QApplication>
#include <QSettings>
#include "database/connection.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("wyCompany");
    QCoreApplication::setOrganizationDomain("company.wy");
    QCoreApplication::setApplicationName("Spectr");

    QApplication a(argc, argv);

    createConnection();

    SpectrView w;
    w.show();

    qRegisterMetaType<QVector<QPointF> >("QVector<QPointF>");

    return a.exec();
}
