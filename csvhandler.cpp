#include "csvhandler.h"
#include <QtDebug>
#include <QFile>

CsvHandler::CsvHandler() :
    m_fieldSeparator(';'),
    m_textSeparator('"'),
    m_data()
{

}

bool CsvHandler::loadCsv(const QString &filePath)
{
    QFile csvFile(filePath);

    if (!csvFile.open(QIODevice::ReadOnly))
        return false;

    QTextStream fileStream(&csvFile);

    while (!fileStream.atEnd())
    {
        QString line = fileStream.readLine();
        m_data.append(parseLine(line));
    }

    csvFile.close();

    return true;
}

bool CsvHandler::saveCsv(const QString &filePath)
{
    QFile csvFile(filePath);

    if (!csvFile.open(QIODevice::WriteOnly))
        return false;

    QTextStream stream(&csvFile);

    for (int i = 0; i < m_data.size(); i++)
    {
        stream << prepareLineToWrite(m_data[i]);
    }

    csvFile.close();

    return true;
}

QVector<QPointF> CsvHandler::toDataVector(bool *success)
{
    bool error = false;
    bool parseError = false;
    QVector<QPointF> vector(m_data.size());

    for (int i = 0; i < m_data.size(); i++)
    {
        if (error)
            break;

        QStringList line = m_data[i];
        if (line.size() < 2)
            error = true;
        else
        {
            QString tmp = line[0];
            tmp = tmp.replace(',', '.');
            qreal wl = tmp.toDouble(&parseError);

            if (!parseError)
            {
                error = true;
                break;
            }

            tmp = line[1];
            tmp = tmp.replace(',', '.');
            qreal power = tmp.toDouble(&parseError);

            if (!parseError)
            {
                error = true;
                break;
            }

            vector[i] = QPointF(wl, power);
        }
    }

    if (success != 0)
        *success = !error;

    if (error)
        return QVector<QPointF>();

    return vector;
}

void CsvHandler::toCsv(const QVector<QPointF> &data)
{
    m_data.clear();

    for (int i = 0; i < data.size(); i++)
    {
        QStringList line;
        QPointF p = data[i];


        QString tmp = QString::number(p.x(), 'f', 10);
        tmp = tmp.replace('.', ',');
        line.append(tmp);

        tmp = QString::number(p.y(), 'f', 10);
        tmp = tmp.replace('.', ',');
        line.append(tmp);

        m_data.append(line);
    }
}

QChar CsvHandler::fieldSeparator() const
{
    return m_fieldSeparator;
}

void CsvHandler::setFieldSeparator(const QChar &fieldSeparator)
{
    m_fieldSeparator = fieldSeparator;
}

QChar CsvHandler::textSeparator() const
{
    return m_textSeparator;
}

void CsvHandler::setTextSeparator(const QChar &textSeparator)
{
    m_textSeparator = textSeparator;
}

QList<QStringList> CsvHandler::data() const
{
    return m_data;
}

void CsvHandler::setData(const QList<QStringList> &data)
{
    m_data = data;
}

QStringList CsvHandler::parseLine(const QString &lineToParse)
{
    QString line = lineToParse + ";";
    QStringList list;
    QString field = "";

    bool onText = false;

    for (int i = 0; i < line.size(); i++)
    {
        QChar c = line[i];

        if (c == m_fieldSeparator && !onText)
        {
            if (!onText)
            {
                list.append(field);
                field = "";
            }
            else
                field += c;
        }
        else if (c == m_textSeparator)
        {
            if (!onText)
                onText = true;
            else
            {
                if (line[i + 1] == m_fieldSeparator)
                {
                    field = removeDoubleTextSerparator(field);
                    onText = false;
                }
                else
                    field += c;
            }
        }
        else
            field += c;
    }

    return list;
}

QString CsvHandler::removeDoubleTextSerparator(QString &field)
{
    QString before(m_textSeparator);
    before += m_textSeparator;

    QString after(m_textSeparator);

    return field.replace(before, after);
}

QString CsvHandler::prepareLineToWrite(QStringList list)
{
    QString singleTextSep(m_textSeparator);
    QString doubleTextSep = singleTextSep + m_textSeparator;

    for (int i = 0; i < list.size(); i++)
    {
        QString item = list[i];
        if (item.contains(m_textSeparator))
        {
            item = item.replace(singleTextSep, doubleTextSep);
            item = m_textSeparator + item + m_textSeparator;
            list[i] = item;
        }
        else if (item.contains(m_fieldSeparator))
        {
            item = m_textSeparator + item + m_textSeparator;
            list[i] = item;
        }
    }

    return list.join(m_fieldSeparator) + "\n";
}

