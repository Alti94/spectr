#ifndef _SWDLL_LIB_H
#define _SWDLL_LIB_H

#ifdef __cplusplus
        extern "C" {
#endif
        int Sinit();
        int Sclose();
        int SscanLV(int chan, float * dataBuffer);
        int Sdevcnt();
        int Srate(int dsf);
        int Smode(int xtmode);
        int Supdate(int scansToAvg, int xsmooth, int tempComp);
        //int Sxtrate(int dsf);
#ifdef __cplusplus
        }
#endif

#endif
