#undef UNICODE
#include <windows.h>
#include "swdll.h"

HINSTANCE hUsbdrvdLib = LoadLibrary("./lib/USBDRVD.DLL");
HINSTANCE hFtd2xxLib = LoadLibrary("./lib/FTD2XX.dll");
HINSTANCE hSwdllLib = LoadLibrary("./lib/Swdll.dll");

typedef void SWDinit();
typedef int SWDdevcnt();
typedef int SWDscanLV( int chan, float *buffer);
typedef void SWDclose();
typedef void SWDrate(int dsf);
typedef void SWDxtmode(int xtmode);
typedef void SWDupdate(int scansToAvg, int xsmooth, int tempComp);

int Sinit() {
    if (hSwdllLib == 0)
        return -2;

    SWDinit* init = (SWDinit*) GetProcAddress (hSwdllLib , "SWDinit");
    if (init != 0)
    {
        (*init)();
        return 0;
    }

    return -1;
}

int Sclose() {
    if (hSwdllLib == 0)
        return -2;

    SWDclose* close = (SWDclose*) GetProcAddress (hSwdllLib , "SWDclose");
    if (close != 0)
    {
        (*close)();
        return 0;
    }

    return -1;
}

int SscanLV(int chan, float * dataBuffer) {
    if (hSwdllLib == 0)
        return -2;


    SWDscanLV* scanLV = (SWDscanLV*) GetProcAddress(hSwdllLib, "SWDscanLV");
    if (scanLV != 0)
        return (*scanLV)(chan, dataBuffer);

    return -1;
}

int Sdevcnt() {
    if (hSwdllLib == 0)
        return -2;

    SWDdevcnt* devcnt = (SWDdevcnt*) GetProcAddress(hSwdllLib, "SWDdevcnt");
    if (devcnt != 0)
        return (*devcnt)();

    return -1;
}

int Srate(int dsf) {
    if (hSwdllLib == 0)
        return -2;

    SWDrate *rate = (SWDrate*) GetProcAddress(hSwdllLib, "SWDrate");
    if (rate != 0) {
        (*rate)(dsf);
        return 0;
    }

    return -1;
}

int Smode(int xtmode) {
    if (hSwdllLib == 0)
        return -2;

    SWDxtmode *mode = (SWDxtmode*) GetProcAddress(hSwdllLib, "SWDxtmode");
    if (mode != 0) {
        (*mode)(xtmode);
        return 0;
    }

    return -1;
}

int Supdate(int scansToAvg, int xsmooth, int tempComp) {
    if (hSwdllLib == 0)
        return -2;

    SWDupdate *update = (SWDupdate*) GetProcAddress(hSwdllLib, "SWDupdate");
    if (update != 0) {
        (*update)(scansToAvg, xsmooth, tempComp);
        return 0;
    }

    return -1;
}
