#include "spectrview.h"
#include "ui_spectrview.h"
#include <swdll.h>
#include <QtDebug>
#include "measureview.h"
#include "widgets/csvfiledialog.h"
#include "widgets/calibrationdialog.h"
#include "csvhandler.h"
#include "database/rows/measurerow.h"
#include "widgets/about.h"

SpectrView::SpectrView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SpectrView),
    m_dataListener(new SpectDataListener(1)),
    m_settingsDialog(new SettingsDialog(this))
{
    ui->setupUi(this);
    Sinit();


    setWindowTitle(QCoreApplication::applicationName());

    applySettings();
    for (int i = 0; i < SWDLL_DATA_SIZE; i++)
    {
        m_zeroDark[i] = 0;
        m_calibration[i] = 1;
    }

    ui->spectrumChartView->showSpectralBg(false);
    ui->spectrumChartView->showCallouts(false);

    connect(m_dataListener, SIGNAL(newRead(float*)) , this, SLOT(updateData(float*)));
    connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(reloadSaved(int)));

    m_dataListener->start();
}

void SpectrView::updateData(float *data)
{
    QVector<QPointF> processedData(SWDLL_DATA_SIZE);
    qreal max = ui->spectrumChartView->maxY();
    qreal maxBefore = max;

    for (int i = 0; i < SWDLL_DATA_SIZE; i++)
    {
        m_wave[i] = data[i];
        qreal y = (data[i] - m_zeroDark[i]) * m_calibration[i];
        if (y < 0) y = 0;

        processedData[i] = QPointF(m_waveLenghts[i], y);
        if (y > max)
            max = y;
    }

    if (max > maxBefore)
    {
        ui->spectrumChartView->setMaxY(max);
        QSettings settings;
        settings.setValue("config/maxY", max);
    }


    ui->spectrumChartView->setData(processedData);
}

void SpectrView::importCsvFile(const QString &path, const QChar &textSeparator, const QChar &fieldSeparator)
{
    CsvHandler handler;
    handler.setTextSeparator(textSeparator);
    handler.setFieldSeparator(fieldSeparator);
    if (!handler.loadCsv(path))
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("Import error!");
        msgBox.setInformativeText("Cannot import measure!");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
        return;
    }

    bool ok = false;

    QVector<QPointF> data = handler.toDataVector(&ok);

    if (!ok)
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("Import error!");
        msgBox.setInformativeText("Cannot import measure!");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
        return;
    }

    MeasureView *measureWindow = new MeasureView(data, path, false);
    measureWindow->show();
}

void SpectrView::reloadSaved(int tab)
{
    if (tab == 1)
        ui->saved->refresh();
}

void SpectrView::countWLs()
{
    QSettings settings;

    qreal   C1 = settings.value("config/C1", (float) 0.787032).toFloat(),
            C2 = settings.value("config/C2", (float) -0.000158).toFloat(),
            C3 = settings.value("config/C3", (float) 252.34).toFloat();

    m_calibrationVector.clear();

    for (int i = 0; i < SWDLL_DATA_SIZE; i++)
    {
        m_waveLenghts[i] = C3 + C1/2*i + C2/4 * i * i;
        m_calibrationVector.append(QPointF(m_waveLenghts[i], 1));
        m_calibration[i] = 1;
    }

    ui->spectrumChartView->setRangeX(m_waveLenghts[0], m_waveLenghts[SWDLL_DATA_SIZE - 1]);
}

void SpectrView::setMaxY(qreal max)
{
    ui->spectrumChartView->setMaxY(max);
}

void SpectrView::applySettings()
{
    countWLs();
    m_dataListener->setIntegerTime(m_settingsDialog->getIntegration());
    m_dataListener->setScansToAvg(m_settingsDialog->getScansToAvg());
    m_dataListener->setXSmooth(m_settingsDialog->getSmoothing());
    m_dataListener->setTempComp(m_settingsDialog->getTempComp());
    m_dataListener->setMode(m_settingsDialog->getXTiming());
    ui->spectrumChartView->setMaxY(m_settingsDialog->getMaxY());
}

SpectrView::~SpectrView()
{
    delete m_dataListener;
    Sclose();
    delete ui;
}

void SpectrView::on_action_darkZero_triggered()
{
    for (int i = 0; i < SWDLL_DATA_SIZE; i++)
        m_zeroDark[i] = m_wave[i];
}

void SpectrView::on_action_takeSnapshot_triggered()
{
    QVector<QPointF> data = ui->spectrumChartView->data();
    if (data.size() > 0)
    {
        MeasureView *measureWindow = new MeasureView(data);
        measureWindow->show();
    }

}

void SpectrView::on_action_showSpectrumColor_toggled(bool arg1)
{
    ui->spectrumChartView->showSpectralBg(arg1);
}

void SpectrView::on_actionImport_CSV_file_triggered()
{
    CsvFileDialog *dialog = new CsvFileDialog("Import CSV file", QFileDialog::AcceptOpen);
    dialog->show();
    connect(dialog, SIGNAL(csvSelected(QString,QChar,QChar)), this, SLOT(importCsvFile(QString,QChar,QChar)));
}

void SpectrView::on_actionSettings_triggered()
{
    if (m_settingsDialog->exec() == QDialog::Accepted)
    {
        applySettings();
    }
}

void SpectrView::on_actionCalibrate_triggered()
{
    QVector<QPointF> data(SWDLL_DATA_SIZE);
    for (int i = 0; i < SWDLL_DATA_SIZE; i++)
    {
        qreal y = m_wave[i] - m_zeroDark[i];
        if (y < 0) y = 0;

        data[i] = QPointF(m_waveLenghts[i], y);
    }

    CalibrationDialog dialog(data, this);
    if (dialog.exec() == QDialog::Accepted)
    {
        m_calibrationVector = dialog.calibCoeff();
        for (int i = 0; i < data.size(); i++)
            m_calibration[i] = m_calibrationVector[i].y();
    }
}

void SpectrView::on_actionCalibration_data_triggered()
{
    SpectrumChart *dialog = new SpectrumChart(this);
    dialog->setWindowFlags(Qt::Dialog);
    dialog->setAttribute(Qt::WA_DeleteOnClose);
    dialog->setWindowTitle("Calibration data");
    dialog->chart()->setMinimumSize(400, 300);
    dialog->resize(600, 400);
    dialog->setData(m_calibrationVector);
    dialog->setMaxY(1);
    dialog->setRangeX(m_calibrationVector.first().x(), m_calibrationVector.last().x());
    dialog->showCallouts(false);
    dialog->showSpectralBg(false);
    dialog->show();
}

void SpectrView::on_actionCalibration_reset_triggered()
{
    for (int i = 0; i < m_calibrationVector.size(); i++)
    {
        m_calibration[i] = 1;
        m_calibrationVector[i].setY(1);
    }
}

void SpectrView::on_actionAbout_triggered()
{
    About dialog(this);
    dialog.exec();
}
