#ifndef DATALISTENER_H
#define DATALISTENER_H

#include <QThread>
#include <swdll.h>
#include <QVector>
#include <QPointF>
#include <QMutex>

#define SWDLL_BUF_SIZE 2051
#define SWDLL_DATA_SIZE 2048

class SpectDataListener : public QThread
{
    Q_OBJECT
public:
    enum XSMOOTH
    {
        NONE = 0,
        SMOOTH_5_PIXEL,
        SMOOTH_9_PIXEL,
        SMOOTH_17_PIXEL,
        SMOOTH_33_PIXEL
    };

    enum XTIMING
    {
        LOW = 0,
        MEDIUM,
        HIGH
    };

    explicit SpectDataListener(int channel, QObject *parent = 0);
    ~SpectDataListener();
    void setIntegerTime(const int &integerTime);
    void setScansToAvg(const int &scansToAvg);
    void setXSmooth(const XSMOOTH &xSmooth);
    void setTempComp(const bool &tempComp);
    void setMode(const XTIMING &mode);
    void setChannel(const int &channel);
    int channel() const;



signals:
    void newRead(float *pointer);
    void closed();

private:
    int m_channel;
    float *m_dataBuffer;

    int m_integerTime = 100; // detector integration rate in ms (4 bis 65500)
    int m_scansToAvg = 1; // ScansToAverage ( 1 .. 99 )
    XSMOOTH m_xsmooth = SMOOTH_5_PIXEL; // xsmooth = (0=none, 1=5_pixel, 2=9_pixel, 3=17_pixel, 4=33_pixel)
    XTIMING m_mode = LOW;
    bool m_tempComp = false; // TempComp = 0=none 1=on
    int m_timeout = 0;
    bool m_close = false;


    void readSpectrometer();
    void setParams();
    QMutex locker;


protected:
    void run();
};

#endif // DATALISTENER_H
