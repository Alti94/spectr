#include "spectdatalistener.h"
#include <QVector>
#include <QPointF>
#include <QEventLoop>

SpectDataListener::SpectDataListener(int channel, QObject *parent) :
    QThread(parent),
    m_channel(channel),
    m_dataBuffer(new float[SWDLL_BUF_SIZE])
{
    for (int i = 0; i < SWDLL_DATA_SIZE; i++)
        m_dataBuffer[i] = 0;
}

void SpectDataListener::readSpectrometer() {
    m_timeout = m_integerTime;

//    do
//    {
//        if (m_close)
//            return;

//        msleep(30);
//        m_timeout -= 30;
//        if (m_timeout <= 0)
//            for (int i = 0; i < SWDLL_DATA_SIZE; i++)
//                m_dataBuffer[i] = (qreal) qrand() / (RAND_MAX / 2);
//    }
//    while(m_timeout > 0);

    int scode = 0;

    do
    {
        if (m_close)
            return;

        locker.lock();
        scode = SscanLV(m_channel, m_dataBuffer);
        locker.unlock();

        if (scode == 0)
            break;
        else
            msleep(30);

        if (m_timeout <= 0)
            return;

        m_timeout -= 30;
    }
    while (scode != 0);

    emit newRead(m_dataBuffer);
}

void SpectDataListener::setIntegerTime(const int &integerTime)
{
    if (integerTime >= 4 && integerTime <= 65500)
        m_integerTime = integerTime;
}

void SpectDataListener::setScansToAvg(const int &scansToAvg)
{
    if (scansToAvg > 0 && scansToAvg < 100)
        m_scansToAvg = scansToAvg;
    setParams();
}

void SpectDataListener::setXSmooth(const XSMOOTH &xSmooth)
{
    m_xsmooth = xSmooth;
    setParams();
}

void SpectDataListener::setTempComp(const bool &tempComp)
{
    m_tempComp = tempComp;
    setParams();
}

void SpectDataListener::setChannel(const int &channel)
{
    m_channel = channel;
}

int SpectDataListener::channel() const
{
    return m_channel;
}

void SpectDataListener::setMode(const XTIMING &mode)
{
    m_mode = mode;
    locker.lock();
    Smode(mode);
    locker.unlock();
}

void SpectDataListener::setParams()
{
    locker.lock();
    Srate(m_integerTime);
    Supdate(m_scansToAvg, m_xsmooth, (m_tempComp) ? 1 : 0);
    locker.unlock();
}

SpectDataListener::~SpectDataListener() {
    m_close = true;

    QEventLoop loop;
    connect(this, SIGNAL(closed()), &loop, SLOT(quit()));
    loop.exec();

    delete[] m_dataBuffer;
}

void SpectDataListener::run()
{
    emit newRead(m_dataBuffer);

    while (!m_close)
    {
        readSpectrometer();
    }

    emit closed();
}
