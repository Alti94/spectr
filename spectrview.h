#ifndef SPECTRVIEW_H
#define SPECTRVIEW_H

#include <QMainWindow>
#include "spectdatalistener.h"
#include "widgets/settingsdialog.h"

namespace Ui {
class SpectrView;
}

class SpectrView : public QMainWindow
{
    Q_OBJECT

public:
    explicit SpectrView(QWidget *parent = 0);
    ~SpectrView();

private:
    Ui::SpectrView *ui;
    SpectDataListener *m_dataListener;
    float m_wave[SWDLL_DATA_SIZE];
    float m_zeroDark[SWDLL_DATA_SIZE];
    float m_waveLenghts[SWDLL_DATA_SIZE];
    float m_calibration[SWDLL_DATA_SIZE];
    QVector<QPointF> m_calibrationVector;
    SettingsDialog *m_settingsDialog;

    void countWLs();
    void setMaxY(qreal max);
    void applySettings();

private slots:
    void updateData(float *data);
    void importCsvFile(const QString &path, const QChar &textSeparator, const QChar &fieldSeparator);
    void reloadSaved(int tab);

    void on_action_darkZero_triggered();
    void on_action_takeSnapshot_triggered();
    void on_action_showSpectrumColor_toggled(bool arg1);
    void on_actionImport_CSV_file_triggered();
    void on_actionSettings_triggered();
    void on_actionCalibrate_triggered();
    void on_actionCalibration_data_triggered();
    void on_actionCalibration_reset_triggered();
    void on_actionAbout_triggered();
};

#endif // SPECTRVIEW_H
