#ifndef CALIBRATIONMANAGER_H
#define CALIBRATIONMANAGER_H

#include <QtCore>

class CalibrationManager
{
public:
    static bool insertCalibration(const QString &name, const QVector<QPointF> &data);
    static bool deleteCalibration(const int &id);

    static QList<QPair<int, QString>> getCalibrations();
    static QVector<QPointF> getCalibrationData(const int &id);
private:
    CalibrationManager();
};

#endif // CALIBRATIONMANAGER_H
