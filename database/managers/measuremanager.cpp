#include "measuremanager.h"

#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlDatabase>
#include <QSqlError>
#include <QtDebug>

MeasureManager::MeasureManager()
{
}

bool MeasureManager::insertMeasure(MeasureRow &row, const QVector<QPointF> &data)
{
    QSqlDatabase db = QSqlDatabase::database();
    if (!db.transaction())
        return false;

    QSqlQuery measureQuery;
    measureQuery.prepare("INSERT INTO measure (name, date, miniature) "
                  "VALUES (:name, :date, :miniature)");
    measureQuery.bindValue(":name", row.name());
    measureQuery.bindValue(":date", row.date());

    QByteArray imgArray;
    QBuffer buffer(&imgArray);
    buffer.open(QIODevice::WriteOnly);
    row.miniature().save(&buffer, "PNG");

    measureQuery.bindValue(":miniature", imgArray);

    if (!measureQuery.exec())
    {
        db.rollback();
        return false;
    }

    QSqlQuery query;
    query.exec("SELECT seq FROM sqlite_sequence WHERE name = 'measure'");
    query.next();

    row.setId(query.value(0).toInt());

    QSqlQuery dataQuery;
    dataQuery.prepare("INSERT INTO measure_data "
               "VALUES (?, ?, ?)");

    QVariantList idList, wlList, pList;

    for (int i = 0; i < data.size(); i++)
    {
        idList.append(row.id());
        wlList.append(data[i].x());
        pList.append(data[i].y());
    }

    dataQuery.addBindValue(idList);
    dataQuery.addBindValue(wlList);
    dataQuery.addBindValue(pList);
    if (!dataQuery.execBatch())
    {
        db.rollback();
        return false;
    }

    db.commit();
    return true;
}

bool MeasureManager::deleteMeasure(const MeasureRow &row)
{
    return deleteMeasure(row.id());
}

bool MeasureManager::deleteMeasure(int id)
{
    QSqlDatabase db = QSqlDatabase::database();
    if (!db.transaction())
        return false;

    QSqlQuery deleteQuery;
    deleteQuery.prepare("DELETE FROM measure WHERE id = :id");
    deleteQuery.bindValue(":id", id);
    if (!deleteQuery.exec())
    {
        db.rollback();
        return false;
    }

    QSqlQuery deleteQuery2;
    deleteQuery2.prepare("DELETE FROM measure_data WHERE measure_id = :id");
    deleteQuery2.bindValue(":id", id);
    if (!deleteQuery2.exec())
    {
        db.rollback();
        return false;
    }

    db.commit();
    return true;
}

QList<MeasureRow> MeasureManager::getMeasures(int page, int count)
{
    QList<MeasureRow> list;

    QSqlQuery query;
    if (count > 0 && page >= 0)
    {
        query.prepare("SELECT * FROM measure ORDER BY id DESC LIMIT :limit OFFSET :offset");
        query.bindValue(":limit", count);
        query.bindValue(":offset", page * count);
    }
    else
    {
        query.prepare("SELECT * FROM measure");
    }

    if (!query.exec())
        return list;

    while (query.next())
    {
        MeasureRow row;
        row.setId(query.value(0).toInt());
        row.setName(query.value(1).toString());
        row.setDate(query.value(2).toDateTime());

        QByteArray imgArray = query.value(3).toByteArray();
        QPixmap pixmap = QPixmap();
        pixmap.loadFromData(imgArray);
        row.setMiniature(pixmap);

        list.append(row);
    }

    return list;
}

QVector<QPointF> MeasureManager::getMeasureData(const MeasureRow &row)
{
    return getMeasureData(row.id());
}

QVector<QPointF> MeasureManager::getMeasureData(int id)
{
    QList<QPointF> list;

    QSqlQuery query;
    query.prepare("SELECT * FROM measure_data WHERE measure_id = :id");
    query.bindValue(":id", id);

    if (!query.exec())
        return list.toVector();

    while (query.next())
    {
        QPointF point;
        point.setX(query.value(1).toDouble());
        point.setY(query.value(2).toDouble());

        list.append(point);
    }

    return list.toVector();
}

int MeasureManager::getMeasuresCount()
{
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM measure");
    query.exec();

    if (query.next())
        return query.value(0).toInt();
    else
        return 0;
}
