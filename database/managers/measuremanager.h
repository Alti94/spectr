#ifndef MEASURETABLE_H
#define MEASURETABLE_H

#include "../rows/measurerow.h"

class MeasureManager
{
public:
    static bool insertMeasure(MeasureRow &row, const QVector<QPointF> &data);
    static bool deleteMeasure(const MeasureRow &row);
    static bool deleteMeasure(int id);
    static QList<MeasureRow> getMeasures(int page = 0, int count = 0);
    static QVector<QPointF> getMeasureData(const MeasureRow &row);
    static QVector<QPointF> getMeasureData(int id);
    static int getMeasuresCount();

private:
    MeasureManager();
};

#endif // MEASURETABLE_H
