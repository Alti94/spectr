#include "calibrationmanager.h"

#include <QSqlDatabase>
#include <QSqlQuery>

bool CalibrationManager::insertCalibration(const QString &name, const QVector<QPointF> &data)
{
    QSqlDatabase db = QSqlDatabase::database();
    if (!db.transaction())
        return false;

    QSqlQuery calibQuery;
    calibQuery.prepare("INSERT INTO calibration (name) "
                       "VALUES (:name)");
    calibQuery.bindValue(":name", name);

    if (!calibQuery.exec()) {
        db.rollback();
        return false;
    }

    QSqlQuery idQuery;
    idQuery.exec("SELECT seq FROM sqlite_sequence WHERE name = 'calibration'");
    idQuery.next();

    int id = idQuery.value(0).toInt();

    QSqlQuery dataQuery;
    dataQuery.prepare("INSERT INTO calibration_data "
                      "VALUES (?, ?, ?)");

    QVariantList idList, wlList, pList;

    for (int i = 0; i < data.size(); i++)
    {
        idList.append(id);
        wlList.append(data[i].x());
        pList.append(data[i].y());
    }

    dataQuery.addBindValue(idList);
    dataQuery.addBindValue(wlList);
    dataQuery.addBindValue(pList);

    if (!dataQuery.execBatch())
    {
        db.rollback();
        return false;
    }

    db.commit();
    return true;
}

bool CalibrationManager::deleteCalibration(const int &id)
{
    QSqlDatabase db = QSqlDatabase::database();
    if (!db.transaction())
        return false;

    QSqlQuery deleteQuery;
    deleteQuery.prepare("DELETE FROM calibration WHERE id = :id");
    deleteQuery.bindValue(":id", id);
    if (!deleteQuery.exec())
    {
        db.rollback();
        return false;
    }

    QSqlQuery deleteQuery2;
    deleteQuery2.prepare("DELETE FROM calibration_data WHERE calibration_id = :id");
    deleteQuery2.bindValue(":id", id);
    if (!deleteQuery2.exec())
    {
        db.rollback();
        return false;
    }

    db.commit();
    return true;
}

QList<QPair<int, QString> > CalibrationManager::getCalibrations()
{
    QList<QPair<int, QString>> list;

    QSqlQuery query;
    query.prepare("SELECT * FROM calibration ORDER BY id DESC");

    if (!query.exec())
        return list;

    while (query.next())
    {
        QPair<int, QString> pair;
        pair.first = query.value(0).toInt();
        pair.second = query.value(1).toString();

        list.append(pair);
    }

    return list;
}

QVector<QPointF> CalibrationManager::getCalibrationData(const int &id)
{
    QList<QPointF> list;

    QSqlQuery query;
    query.prepare("SELECT * FROM calibration_data WHERE calibration_id = :id");
    query.bindValue(":id", id);

    if (!query.exec())
        return list.toVector();

    while (query.next())
    {
        QPointF point;
        point.setX(query.value(1).toDouble());
        point.setY(query.value(2).toDouble());

        list.append(point);
    }

    return list.toVector();
}

CalibrationManager::CalibrationManager()
{

}
