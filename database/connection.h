#ifndef CONNECTION_H
#define CONNECTION_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

static bool createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("database.sqlite");

    if (!db.open())
    {
        return false;
    }

    QSqlQuery query;

    query.exec("CREATE TABLE IF NOT EXISTS measure ("
               "id INTEGER PRIMARY KEY AUTOINCREMENT, "
               "name TEXT, "
               "date TEXT,"
               "miniature BLOB)");

    query.exec("CREATE TABLE IF NOT EXISTS measure_data ("
               "measure_id INTEGER, "
               "wave_lenght REAL, "
               "power REAL, "
               "PRIMARY KEY(measure_id, wave_lenght))");

    query.exec("CREATE TABLE IF NOT EXISTS calibration ("
               "id INTEGER PRIMARY KEY AUTOINCREMENT, "
               "name TEXT)");

    query.exec("CREATE TABLE IF NOT EXISTS calibration_data ("
               "calibration_id INTEGER, "
               "wave_lenght REAL, "
               "power REAL, "
               "PRIMARY KEY(calibration_id, wave_lenght))");

    return true;
}

#endif // CONNECTION_H
