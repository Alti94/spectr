#include "measurerow.h"

MeasureRow::MeasureRow() :
    m_id(-1),
    m_name(""),
    m_date(QDateTime::currentDateTime()),
    m_miniature(QPixmap())
{
}

MeasureRow::MeasureRow(const QString &name, const QPixmap &miniature) :
    m_id(-1),
    m_name(name),
    m_date(QDateTime::currentDateTime()),
    m_miniature(miniature)
{

}

MeasureRow::MeasureRow(const QString &name, const QDateTime &date, const QPixmap &miniature) :
    m_id(-1),
    m_name(name),
    m_date(date),
    m_miniature(miniature)
{
}

MeasureRow::MeasureRow(int id, const QString &name, const QDateTime &date, const QPixmap &miniature) :
    m_id(id),
    m_name(name),
    m_date(date),
    m_miniature(miniature)
{
}

MeasureRow::MeasureRow(const MeasureRow &obj) :
    m_id(obj.m_id),
    m_name(obj.m_name),
    m_date(obj.m_date),
    m_miniature(obj.m_miniature)
{
}

int MeasureRow::id() const
{
    return m_id;
}

void MeasureRow::setId(int id)
{
    m_id = id;
}

QString MeasureRow::name() const
{
    return m_name;
}

void MeasureRow::setName(const QString &name)
{
    m_name = name;
}

QDateTime MeasureRow::date() const
{
    return m_date;
}

void MeasureRow::setDate(const QDateTime &date)
{
    m_date = date;
}

QPixmap MeasureRow::miniature() const
{
    return m_miniature;
}

void MeasureRow::setMiniature(const QPixmap &miniature)
{
    m_miniature = miniature;
}
