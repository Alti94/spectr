#ifndef MEASUREROW_H
#define MEASUREROW_H

#include <QtCore>
#include <QPixmap>

class MeasureRow
{
public:
    MeasureRow();
    explicit MeasureRow(const QString &name, const QPixmap &miniature);
    explicit MeasureRow(const QString &name, const QDateTime &date, const QPixmap &miniature);
    explicit MeasureRow(int id, const QString &name, const QDateTime &date, const QPixmap &miniature);
    MeasureRow(const MeasureRow &obj);

    int id() const;
    void setId(int id);
    QString name() const;
    void setName(const QString &name);
    QDateTime date() const;
    void setDate(const QDateTime &date);
    QPixmap miniature() const;
    void setMiniature(const QPixmap &miniature);

private:
    int m_id;
    QString m_name;
    QDateTime m_date;
    QPixmap m_miniature;
};

#endif // MEASUREROW_H
