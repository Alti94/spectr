#include "spectrumchart.h"
#include <QPainter>
#include <QtDebug>
#include <QGLWidget>

SpectrumChart::SpectrumChart(QWidget *parent) :
    QChartView(parent),
    m_chart(new QChart()),
    m_dataSeries(new QLineSeries()),
    m_dataArea(0),
    m_axisX(new QValueAxis()),
    m_axisY(new QValueAxis()),
    m_spectralPixmap(QPixmap(":/img/spectral_360_830.png")),
    m_callout(0),
    m_showCallouts(true)
{
    setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DirectRendering)));

    m_dataArea = new QAreaSeries(m_dataSeries);

    QPen pen(Qt::black);
    pen.setWidth(2);
    m_dataSeries->setPen(pen);

    setRangeX(360, 830);
    m_axisX->setTickCount(10);

    QSettings settings;

    m_axisY->setRange(0, settings.value("config/maxY", 1).toDouble());
    m_axisY->setTickCount(10);

    m_axisX->setTitleText("Wavelenght in [nm]");

    m_chart->addSeries(m_dataArea);
    m_chart->addSeries(m_dataSeries);
    m_chart->setAxisX(m_axisX, m_dataArea);
    m_chart->setAxisY(m_axisY, m_dataArea);
    m_chart->setAxisX(m_axisX, m_dataSeries);
    m_chart->setAxisY(m_axisY, m_dataSeries);

    m_chart->legend()->hide();
    m_chart->setAcceptHoverEvents(true);

    setMouseTracking(true);
    //setRenderHint(QPainter::Antialiasing, true);

    setChart(m_chart);

    connect(m_dataSeries, SIGNAL(hovered(QPointF,bool)), this, SLOT(showCoords(QPointF,bool)));
}

void SpectrumChart::setData(QVector<QPointF> &data)
{
    m_dataSeries->replace(data);
}

void SpectrumChart::setRangeX(qreal min, qreal max, int ticks)
{
    m_axisX->setRange(min, max);
    m_axisY->setTickCount(ticks);

    QPixmap bgMap(max - min + 1, 1);
    bgMap.fill(Qt::black);

    QPainter painter(&bgMap);
    QRectF srcRect(0, 0, 471, 1);
    QRectF dstRect(360 - min, 0, 471, 1);

    painter.drawPixmap(dstRect, m_spectralPixmap, srcRect);

    QBrush brush;
    brush.setTexture(bgMap);

    m_dataArea->setBrush(brush);
}

void SpectrumChart::resizeEvent(QResizeEvent *event)
{
    QChartView::resizeEvent(event);

    QTransform transform;
    QRectF rect = m_chart->plotArea();
    QBrush brush = m_dataArea->brush();
    QPixmap tmpPixmap = brush.texture();
    transform.scale(rect.width() / tmpPixmap.width(), rect.height() / tmpPixmap.height());

    brush.setTransform(transform);
    m_dataArea->setBrush(brush);
}

void SpectrumChart::showSpectralBg(bool spectralBg)
{
    m_dataArea->setVisible(spectralBg);
}

void SpectrumChart::showCoords(QPointF point, bool state)
{
    if (m_showCallouts)
    {
        if (m_callout == 0)
            m_callout = new Callout(m_chart);

        if (state) {
            m_callout->setText(QString("Wavelenght: %1 nm \nCount: %2 ").arg(point.x(), 0, 'f', 2).arg(point.y(), 0, 'f', 5));
            m_callout->setAnchor(point);
            m_callout->setZValue(11);
            m_callout->updateGeometry();
            m_callout->show();
        } else {
            m_callout->hide();
        }
    }
}

void SpectrumChart::showCallouts(bool callouts)
{
    m_showCallouts = callouts;
}

void SpectrumChart::setMaxY(qreal max, int ticks)
{
    if (max > 0)
    {
        QSettings settings;
        m_axisY->setMax(max);
        settings.setValue("config/maxY", max);
    }
    m_axisY->setTickCount(ticks);
}

qreal SpectrumChart::maxY() const
{
    return m_axisY->max();
}

QChart *SpectrumChart::chart() const
{
    return m_chart;
}

QValueAxis *SpectrumChart::axisX() const
{
    return m_axisX;
}

QValueAxis *SpectrumChart::axisY() const
{
    return m_axisY;
}

QVector<QPointF> SpectrumChart::data() const
{
    return m_dataSeries->pointsVector();
}
