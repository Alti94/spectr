#ifndef SAVEDIALOG_H
#define SAVEDIALOG_H

#include <QDialog>

namespace Ui {
class SaveDialog;
}

class SaveDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SaveDialog(QWidget *parent = 0);
    QString getName() const;
    ~SaveDialog();

private slots:
    void on_nameEdit_textChanged(const QString &arg1);

private:
    Ui::SaveDialog *ui;
};

#endif // SAVEDIALOG_H
