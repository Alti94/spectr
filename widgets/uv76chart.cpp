#include "uv76chart.h"
#include "callout.h"

Uv76Chart::Uv76Chart(QWidget *parent) :
    QChartView(parent),
    m_lineSeries(new QLineSeries()),
    m_uv76Point(new QScatterSeries()),
    m_blackBodySeries(new QSplineSeries()),
    m_chart(new QChart()),
    m_bgPixmap(QPixmap(":/img/uv76_colorSpace")),
    m_posBox(0),
    m_markLines(QVector<QGraphicsLineItem*>(16)),
    m_markLabels(QVector<QGraphicsTextItem*>(9))
{
    QBrush pointBrush(Qt::red);
    QPen pointPen(Qt::black);
    QPen outlinePen(Qt::black);
    pointPen.setWidth(1);
    outlinePen.setWidth(2);

    m_lineSeries->replace(m_uv76chroma);
    m_lineSeries->setPen(outlinePen);

    QPen bbPen(Qt::black);
    bbPen.setWidth(1);

    m_blackBodySeries->replace(m_blackBodyLocus);
    m_blackBodySeries->setPen(bbPen);

    m_chart->addSeries(m_lineSeries);
    m_chart->addSeries(m_uv76Point);
    m_chart->addSeries(m_blackBodySeries);

    m_uv76Point->setMarkerShape(QScatterSeries::MarkerShapeRectangle);
    m_uv76Point->setMarkerSize(6);
    m_uv76Point->setPen(pointPen);
    m_uv76Point->setBrush(pointBrush);

    QValueAxis *axisX = new QValueAxis();
    axisX->setRange(0, 0.65);
    axisX->setTickCount(14);
    axisX->setLabelFormat("%.2f");
    axisX->setTitleText("u'");

    QValueAxis *axisY = new QValueAxis();
    axisY->setRange(0, 0.65);
    axisY->setTickCount(14);
    axisY->setLabelFormat("%.2f");
    axisY->setTitleText("v'");

    QColor color = axisY->gridLineColor();
    color.setAlpha(20);
    axisX->setGridLineColor(color);
    axisY->setGridLineColor(color);

    m_chart->setAxisX(axisX, m_lineSeries);
    m_chart->setAxisY(axisY, m_lineSeries);
    m_chart->setAxisX(axisX, m_uv76Point);
    m_chart->setAxisY(axisY, m_uv76Point);
    m_chart->setAxisX(axisX, m_blackBodySeries);
    m_chart->setAxisY(axisY, m_blackBodySeries);
    m_chart->legend()->hide();

    QBrush bgBrush;
    bgBrush.setTexture(m_bgPixmap);
    m_chart->setPlotAreaBackgroundBrush(bgBrush);
    m_chart->setPlotAreaBackgroundVisible();

    initMarks();

    setChart(m_chart);

    connect(m_uv76Point, SIGNAL(hovered(QPointF,bool)), this, SLOT(showCoords(QPointF,bool)));
}

Uv76Chart::~Uv76Chart()
{
}

void Uv76Chart::showCoords(QPointF point, bool state)
{
    if (m_posBox == 0)
        m_posBox = new Callout(m_chart);

    if (state) {
        m_posBox->setText(QString("u': %1 \nv': %2 ").arg(point.x(), 0, 'g', 6).arg(point.y(), 0, 'g', 6));
        m_posBox->setAnchor(point);
        m_posBox->setZValue(11);
        m_posBox->updateGeometry();
        m_posBox->show();
    } else {
        m_posBox->hide();
    }
}

void Uv76Chart::resizeEvent(QResizeEvent *event)
{
    QChartView::resizeEvent(event);

    QTransform transform;
    QRectF rect = m_chart->plotArea();
    transform.translate(rect.x(), rect.y());
    transform.scale(rect.width() / m_bgPixmap.width(), rect.height() / m_bgPixmap.height());
    QBrush bgBrush = m_chart->plotAreaBackgroundBrush();
    bgBrush.setTransform(transform);
    m_chart->setPlotAreaBackgroundBrush(bgBrush);

    resizeMarks();
}

void Uv76Chart::showGrid(bool grid)
{
    m_chart->axisX()->setGridLineVisible(grid);
    m_chart->axisY()->setGridLineVisible(grid);
}

void Uv76Chart::fillColor(bool color)
{
    m_chart->setPlotAreaBackgroundVisible(color);
}

void Uv76Chart::showBbLocus(bool bbLocus)
{
    m_blackBodySeries->setVisible(bbLocus);

    for (int i = 0; i < 16; i++)
        m_markLines[i]->setVisible(bbLocus);

    for (int i = 0; i < 9; i++)
        m_markLabels[i]->setVisible(bbLocus);
}

void Uv76Chart::setPoint(const QPointF &point)
{
    m_uv76Point->clear();
    m_uv76Point->append(point);
}

void Uv76Chart::initMarks()
{
    for (int i = 0; i < 16; i++)
    {
        m_markLines[i] = new QGraphicsLineItem(m_chart);
        m_markLines[i]->setZValue(3);
    }

    const int tab[9] = { 0, 1, 2, 3, 4, 6, 9, 13, 15};
    for (int i = 0; i < 9; i++)
    {
        m_markLabels[i] = new QGraphicsTextItem(m_tempMarks[tab[i]].temp, m_chart);
        m_markLabels[i]->setZValue(3);
    }
}

void Uv76Chart::resizeMarks()
{
    for (int i = 0; i < 16; i++)
    {
        QPointF start(m_chart->mapToPosition(m_tempMarks[i].start, m_lineSeries));
        QPointF end(m_chart->mapToPosition(m_tempMarks[i].end, m_lineSeries));
        QLineF line(start, end);
        m_markLines[i]->setLine(line);
    }

    const int tab[9] = { 0, 1, 2, 3, 4, 6, 9, 13, 15};
    for (int i = 0; i < 9; i++)
    {
        QPointF pos(m_chart->mapToPosition(m_tempMarks[tab[i]].end));
        m_markLabels[i]->setPos(pos);
    }
}

const QVector<QPointF> Uv76Chart::m_uv76chroma = QVector<QPointF>({
                                                                QPointF(0.258899676375404, 0.0175654162649387),
                                                                QPointF(0.258278099636257, 0.0174387792715411),
                                                                QPointF(0.257751892836342, 0.0173185789167152),
                                                                QPointF(0.257278900944501, 0.0171883537584888),
                                                                QPointF(0.256865194076159, 0.0164765502779115),
                                                                QPointF(0.256672435865611, 0.0165298885351735),
                                                                QPointF(0.256401618115986, 0.0163159172498978),
                                                                QPointF(0.255991968879408, 0.0163383109549504),
                                                                QPointF(0.2557640407823, 0.0159249308411621),
                                                                QPointF(0.255262940642285, 0.0158507388755882),
                                                                QPointF(0.254496534378381, 0.0159243119936829),
                                                                QPointF(0.253645148378981, 0.0160264002679235),
                                                                QPointF(0.252217081616757, 0.016892050413386),
                                                                QPointF(0.249629491637508, 0.0190909549757697),
                                                                QPointF(0.246083169003402, 0.0226233557977767),
                                                                QPointF(0.241101802208448, 0.0278092763643169),
                                                                QPointF(0.234750929317916, 0.0348810169754283),
                                                                QPointF(0.226643615514597, 0.0436604448090954),
                                                                QPointF(0.216117881311942, 0.0549615670796284),
                                                                QPointF(0.203284962525913, 0.0688885345239994),
                                                                QPointF(0.187661331956634, 0.0871192565823439),
                                                                QPointF(0.168979962650785, 0.111896229748145),
                                                                QPointF(0.144097894515562, 0.15099078366507),
                                                                QPointF(0.114670755326017, 0.204446417043254),
                                                                QPointF(0.0828089534713297, 0.27083047470791),
                                                                QPointF(0.052136174689587, 0.342708695712579),
                                                                QPointF(0.0281539628615719, 0.411662653781457),
                                                                QPointF(0.0118701550387597, 0.46984011627907),
                                                                QPointF(0.00345929155120987, 0.513069415273831),
                                                                QPointF(0.00142247510668563, 0.543163229018492),
                                                                QPointF(0.00463326233979748, 0.563838134738258),
                                                                QPointF(0.0122691626612699, 0.576966860612193),
                                                                QPointF(0.0231165088019108, 0.583667185254504),
                                                                QPointF(0.0359953527010998, 0.586139652971284),
                                                                QPointF(0.0500681441054971, 0.586750244668348),
                                                                QPointF(0.064325275925281, 0.586525028377575),
                                                                QPointF(0.079228990607927, 0.585623375826155),
                                                                QPointF(0.0952570112020339, 0.584114562643998),
                                                                QPointF(0.112701838120608, 0.582070930212781),
                                                                QPointF(0.131892640827752, 0.579549621926532),
                                                                QPointF(0.153111156897085, 0.576581333058617),
                                                                QPointF(0.176601705787247, 0.573187597122124),
                                                                QPointF(0.202573031019909, 0.569362855851785),
                                                                QPointF(0.231155985705369, 0.565104363154971),
                                                                QPointF(0.262338731266922, 0.560436610777038),
                                                                QPointF(0.295933410446576, 0.555419473361911),
                                                                QPointF(0.331476188169178, 0.550118695799622),
                                                                QPointF(0.368085551065905, 0.544630416608611),
                                                                QPointF(0.403510104847288, 0.539336726941194),
                                                                QPointF(0.437975160743083, 0.534190629777787),
                                                                QPointF(0.469128350540789, 0.529559156916555),
                                                                QPointF(0.496697154471545, 0.525438262195122),
                                                                QPointF(0.520211453240804, 0.521916227377028),
                                                                QPointF(0.539924910629278, 0.518978928269891),
                                                                QPointF(0.556485582184642, 0.516507671820661),
                                                                QPointF(0.570873244333011, 0.514354791558577),
                                                                QPointF(0.583020930959075, 0.512535145267104),
                                                                QPointF(0.592974858556267, 0.51104637546583),
                                                                QPointF(0.600476568705322, 0.509928514694202),
                                                                QPointF(0.606363069245165, 0.509045539613225),
                                                                QPointF(0.61079729604593, 0.50838040559311),
                                                                QPointF(0.613748575769085, 0.507937713634637),
                                                                QPointF(0.61614381388791, 0.507578427916814),
                                                                QPointF(0.618075801749271, 0.507288629737609),
                                                                QPointF(0.619942340192862, 0.507008648971071),
                                                                QPointF(0.621634388285309, 0.506754841757204),
                                                                QPointF(0.622557422008913, 0.506616386698663),
                                                                QPointF(0.623100752323351, 0.506534887151497),
                                                                QPointF(0.623366217967116, 0.506495067304933),
                                                                QPointF(0.623366183044987, 0.506495072543252),
                                                                QPointF(0.623366123410612, 0.506495081488408),
                                                                QPointF(0.623366192182114, 0.506495071172683),
                                                                QPointF(0.623366166376241, 0.506495075043564),
                                                                QPointF(0.623366154112915, 0.506495076883063),
                                                                QPointF(0.623366025715882, 0.506495096142618),
                                                                QPointF(0.62336613491203, 0.506495079763196),
                                                                QPointF(0.623366170600495, 0.506495074409926),
                                                                QPointF(0.623366155606772, 0.506495076658984),
                                                                QPointF(0.623366184072221, 0.506495072389167),
                                                                QPointF(0.623366160169378, 0.506495075974593),
                                                                QPointF(0.623366025715882, 0.506495096142618),
                                                                QPointF(0.623365936080211, 0.506495109587968),
                                                                QPointF(0.623366152400955, 0.506495077139857),
                                                                QPointF(0.623366115351547, 0.506495082697268),
                                                                QPointF(0.623366111285353, 0.506495083307197),
                                                                QPointF(0.623366115351547, 0.506495082697268),
                                                                QPointF(0.6233660308702, 0.50649509536947),
                                                                QPointF(0.623366086615217, 0.506495087007717),
                                                                QPointF(0.623366122836315, 0.506495081574553),
                                                                QPointF(0.623366130282804, 0.506495080457579),
                                                                QPointF(0.623366149359023, 0.506495077596146),
                                                                QPointF(0.623366180717526, 0.506495072892371),
                                                                QPointF(0.623366112757719, 0.506495083086342),
                                                                QPointF(0.623366073775957, 0.506495088933606),
                                                                QPointF(0.62336604390648, 0.506495093414028),
                                                                QPointF(0.258899676375404, 0.0175654162649387),
                                                            });

const QVector<QPointF> Uv76Chart::m_blackBodyLocus = QVector<QPointF>({
                                                                          QPointF(0.448005849, 0.531938127),
                                                                          QPointF(0.405914181, 0.536920735),
                                                                          QPointF(0.372189411, 0.539780718),
                                                                          QPointF(0.345073137, 0.54078766),
                                                                          QPointF(0.323074505, 0.540281021),
                                                                          QPointF(0.305044889, 0.538598262),
                                                                          QPointF(0.272174289, 0.531100614),
                                                                          QPointF(0.250568277, 0.521383525),
                                                                          QPointF(0.235714683, 0.511266359),
                                                                          QPointF(0.225108997, 0.501579455),
                                                                          QPointF(0.211423335, 0.484672084),
                                                                          QPointF(0.20330711, 0.471175202),
                                                                          QPointF(0.198126692, 0.460537591),
                                                                          QPointF(0.194624135, 0.452113314),
                                                                          QPointF(0.192143499, 0.445367873),
                                                                          QPointF(0.190318447, 0.439896021),
                                                                          QPointF(0.185710446, 0.423510286),
                                                                          QPointF(0.183884583, 0.415633654),
                                                                      });

const QVector<Uv76Chart::TempMark> Uv76Chart::m_tempMarks = QVector<Uv76Chart::TempMark>({
                                                                                             { "1000", QPointF(0.449734043, 0.561825928), QPointF(0.446279212, 0.502050143) },
                                                                                             { "1500", QPointF(0.358243,0.570433), QPointF(0.357576,0.510561)},
                                                                                             { "2000", QPointF(0.303285826, 0.568481887), QPointF(0.306804968, 0.508714668) },
                                                                                             { "2500", QPointF(0.267707014, 0.560342429), QPointF(0.276643169, 0.501859112) },
                                                                                             { "3000", QPointF(0.243441836, 0.549413882), QPointF(0.257696116, 0.493353415) },
                                                                                             { "3500", QPointF(0.226260659, 0.53770048), QPointF(0.245160729, 0.48482068) },
                                                                                             { "4000", QPointF(0.213761074, 0.526282805), QPointF(0.236457659, 0.476876847) },
                                                                                             { "4500", QPointF(0.204463205, 0.51567687), QPointF(0.230157575, 0.469692624) },
                                                                                             { "5000", QPointF(0.197409438, 0.506075251), QPointF(0.225437744, 0.463268984) },
                                                                                             { "6000", QPointF(0.187676864, 0.489892392), QPointF(0.218937411, 0.452458672) },
                                                                                             { "7000", QPointF(0.181491183, 0.477195569), QPointF(0.214760966, 0.443881627) },
                                                                                             { "8000", QPointF(0.177335618, 0.46720736), QPointF(0.21190966, 0.437022794) },
                                                                                             { "9000", QPointF(0.174413877, 0.45926141), QPointF(0.209870333, 0.431477412) },
                                                                                             { "10000", QPointF(0.172280139, 0.45285341), QPointF(0.208356932, 0.426939221) },
                                                                                             { "15000", QPointF(0.166950853, 0.433911802), QPointF(0.204469808, 0.413109126) },
                                                                                             { "20000", QPointF(0.164872373, 0.424952154), QPointF(0.202892579, 0.406317523) },
                                                                                         });
