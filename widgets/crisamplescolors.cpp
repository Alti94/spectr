#include "crisamplescolors.h"
#include "ui_crisamplescolors.h"
#include "../colorimetry/rgbconverter.h"

CriSamplesColors::CriSamplesColors(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CriSamplesColors)
{
    ui->setupUi(this);
}

void CriSamplesColors::setReference(const QVector<CIE_XYZ> &samples, const CIE_XYZ light)
{
    RgbConverter converter(light);

    QWidget* widget[14] = {
        ui->ref_1,
        ui->ref_2,
        ui->ref_3,
        ui->ref_4,
        ui->ref_5,
        ui->ref_6,
        ui->ref_7,
        ui->ref_8,
        ui->ref_9,
        ui->ref_10,
        ui->ref_11,
        ui->ref_12,
        ui->ref_13,
        ui->ref_14,
    };

    if (samples.size() >= 14)
    {
        for (int i = 0; i < 14; i++)
        {
            QPalette pal = palette();
            pal.setColor(QPalette::Background, converter.XYZtoRgb255(samples[i]));

            widget[i]->setAutoFillBackground(true);
            widget[i]->setPalette(pal);
        }
    }
}

void CriSamplesColors::setTest(const QVector<CIE_XYZ> &samples, const CIE_XYZ light)
{
    RgbConverter converter(light);

    QWidget* widget[14] = {
        ui->sam_1,
        ui->sam_2,
        ui->sam_3,
        ui->sam_4,
        ui->sam_5,
        ui->sam_6,
        ui->sam_7,
        ui->sam_8,
        ui->sam_9,
        ui->sam_10,
        ui->sam_11,
        ui->sam_12,
        ui->sam_13,
        ui->sam_14,
    };

    if (samples.size() >= 14)
    {
        for (int i = 0; i < 14; i++)
        {
            QPalette pal = palette();
            pal.setColor(QPalette::Background, converter.XYZtoRgb255(samples[i]));

            widget[i]->setAutoFillBackground(true);
            widget[i]->setPalette(pal);
        }
    }
}

CriSamplesColors::~CriSamplesColors()
{
    delete ui;
}
