#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include "spectdatalistener.h"

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

    qreal getC1() const;
    qreal getC2() const;
    qreal getC3() const;

    SpectDataListener::XSMOOTH getSmoothing() const;
    SpectDataListener::XTIMING getXTiming() const;

    int getIntegration() const;
    int getScansToAvg() const;
    bool getTempComp() const;

    qreal getMaxY() const;

public slots:
    int exec();

private:
    Ui::SettingsDialog *ui;

    void loadSettings();
    void saveSettings();
};

#endif // SETTINGSDIALOG_H
