#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include <QSettings>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    loadSettings();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

qreal SettingsDialog::getC1() const
{
    return ui->spin_c1->value();
}

qreal SettingsDialog::getC2() const
{
    return ui->spin_c2->value();
}

qreal SettingsDialog::getC3() const
{
    return ui->spin_c3->value();
}

SpectDataListener::XSMOOTH SettingsDialog::getSmoothing() const
{
    if (ui->sm_none->isChecked())
        return SpectDataListener::NONE;
    else if (ui->sm_5p->isChecked())
        return SpectDataListener::SMOOTH_5_PIXEL;
    else if (ui->sm_9p->isChecked())
        return SpectDataListener::SMOOTH_9_PIXEL;
    else if (ui->sm_17p->isChecked())
        return SpectDataListener::SMOOTH_17_PIXEL;
    else if (ui->sm_33p->isChecked())
        return SpectDataListener::SMOOTH_33_PIXEL;
    else
        return SpectDataListener::NONE;
}

SpectDataListener::XTIMING SettingsDialog::getXTiming() const
{
    if (ui->xt_1->isChecked())
        return SpectDataListener::LOW;
    else if (ui->xt_2->isChecked())
        return SpectDataListener::MEDIUM;
    else if (ui->xt_3->isChecked())
        return SpectDataListener::HIGH;
    else
        return SpectDataListener::LOW;
}

int SettingsDialog::getIntegration() const
{
    return ui->sp_integ->value();
}

int SettingsDialog::getScansToAvg() const
{
    return ui->sp_avg->value();
}

bool SettingsDialog::getTempComp() const
{
    return ui->tempComp->isChecked();
}

qreal SettingsDialog::getMaxY() const
{
    return ui->maxY->value();
}

int SettingsDialog::exec()
{
    loadSettings();
    int result = QDialog::exec();

    if (result == QDialog::Accepted)
        saveSettings();

    return result;
}

void SettingsDialog::loadSettings()
{
    QSettings settings;

    ui->spin_c1->setValue(settings.value("config/C1", 0.787032).toDouble());
    ui->spin_c2->setValue(settings.value("config/C2", -0.000158).toDouble());
    ui->spin_c3->setValue(settings.value("config/C3", 252.34).toDouble());

    ui->sp_integ->setValue(settings.value("config/integr", 100).toInt());
    ui->sp_avg->setValue(settings.value("config/scansAvg", 1).toInt());
    ui->maxY->setValue(settings.value("config/maxY", 1).toDouble());
    ui->tempComp->setChecked(settings.value("config/tempComp", false).toBool());

    int val = settings.value("config/smoothing", 0).toInt();
    switch (val)
    {
    case SpectDataListener::NONE:
        ui->sm_none->setChecked(true);
        break;
    case SpectDataListener::SMOOTH_5_PIXEL:
        ui->sm_5p->setChecked(true);
        break;
    case SpectDataListener::SMOOTH_9_PIXEL:
        ui->sm_9p->setChecked(true);
        break;
    case SpectDataListener::SMOOTH_17_PIXEL:
        ui->sm_17p->setChecked(true);
        break;
    case SpectDataListener::SMOOTH_33_PIXEL:
        ui->sm_33p->setChecked(true);
        break;
    default:
        ui->sm_none->setChecked(true);
        break;
    }

    val = settings.value("config/xtiming", 0).toInt();

    switch (val)
    {
    case SpectDataListener::LOW:
        ui->xt_1->setChecked(true);
        break;
    case SpectDataListener::MEDIUM:
        ui->xt_2->setChecked(true);
        break;
    case SpectDataListener::HIGH:
        ui->xt_3->setChecked(true);
        break;
    default:
        ui->xt_1->setChecked(true);
        break;
    }
}

void SettingsDialog::saveSettings()
{
    QSettings settings;
    settings.setValue("config/C1", getC1());
    settings.setValue("config/C2", getC2());
    settings.setValue("config/C3", getC3());

    settings.setValue("config/smoothing", getSmoothing());
    settings.setValue("config/xtiming", getXTiming());

    settings.setValue("config/integr", getIntegration());
    settings.setValue("config/scansAvg", getScansToAvg());
    settings.setValue("config/maxY", getMaxY());
    settings.setValue("config/tempComp", getTempComp());
}
