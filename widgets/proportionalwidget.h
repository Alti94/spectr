#ifndef PROPORTIONALWIDGET_H
#define PROPORTIONALWIDGET_H

#include <QWidget>

namespace Ui {
class ProportionalWidget;
}

class ProportionalWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ProportionalWidget(QWidget *parent = 0);
    ~ProportionalWidget();
    void setWidget(QWidget *widget);
    QWidget* widget();
    void setBackgroundColor(const QColor &color);

private:
    Ui::ProportionalWidget *ui;

protected:
    void resizeEvent(QResizeEvent *event);
};

#endif // PROPORTIONALWIDGET_H
