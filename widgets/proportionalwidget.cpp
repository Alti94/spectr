#include "proportionalwidget.h"
#include "ui_proportionalwidget.h"
#include <QResizeEvent>

ProportionalWidget::ProportionalWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProportionalWidget)
{
    ui->setupUi(this);
}

ProportionalWidget::~ProportionalWidget()
{
    delete ui;
}

void ProportionalWidget::resizeEvent(QResizeEvent *event)
{
    QSize size = event->size();

    int square;

    if (size.width() > size.height())
        square = size.height();
    else
        square = size.width();

    ui->widget->setMaximumSize(square, square);
    ui->widget->setFixedSize(square, square);
    ui->widget->setMinimumSize(square, square);

    QWidget::resizeEvent(event);
}

void ProportionalWidget::setWidget(QWidget *widget)
{
    ui->container->replaceWidget(ui->widget, widget);
    ui->widget = widget;
    ui->widget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

QWidget* ProportionalWidget::widget()
{
    return ui->widget;
}

void ProportionalWidget::setBackgroundColor(const QColor &color)
{
    setStyleSheet("background-color: " + color.name());
}
