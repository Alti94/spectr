#include "savedialog.h"
#include "ui_savedialog.h"
#include <QPushButton>

SaveDialog::SaveDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SaveDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(true);
}

QString SaveDialog::getName() const
{
    return ui->nameEdit->text();
}

SaveDialog::~SaveDialog()
{
    delete ui;
}

void SaveDialog::on_nameEdit_textChanged(const QString &arg1)
{
    QString text = arg1.trimmed();
    QPushButton *button = ui->buttonBox->button(QDialogButtonBox::Ok);
    if (text.size() > 0)
        button->setEnabled(true);
    else
        button->setDisabled(true);
}
