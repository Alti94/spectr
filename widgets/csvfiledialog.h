#ifndef CSVFILEDIALOG_H
#define CSVFILEDIALOG_H

#include <QDialog>
#include "customfiledialog.h"

namespace Ui {
class CsvFileDialog;
}

class CsvFileDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CsvFileDialog(const QString &windowTitle, QFileDialog::AcceptMode mode, QWidget *parent = 0);
    void setAcceptMode(QFileDialog::AcceptMode mode);
    QString getFilePath() const;
    ~CsvFileDialog();

    QChar getTextSepartaor() const;
    QChar getFieldsSepartaor() const;

private:
    Ui::CsvFileDialog *ui;
    CustomFileDialog *m_fileDialog;

public slots:

private slots:
    void openFileSlot(const QString &path);
    void fieldsFilled();

signals:
    void csvSelected(const QString &path, const QChar &textSeparator, const QChar &fieldSeparator);
};

#endif // CSVFILEDIALOG_H
