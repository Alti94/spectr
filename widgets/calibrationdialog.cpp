#include "calibrationdialog.h"
#include "ui_calibrationdialog.h"
#include "widgets/csvfiledialog.h"
#include "csvhandler.h"
#include "colorimetry/qubicspline.h"
#include "widgets/savedialog.h"
#include "database/managers/calibrationmanager.h"

CalibrationDialog::CalibrationDialog(const QVector<QPointF> &data, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CalibrationDialog),
    m_spectData(data),
    m_calibCoeff(data.size())
{
    ui->setupUi(this);

    ui->spectrum->setMaxY(1);
    ui->spectrum->showSpectralBg(false);
    ui->spectrum->setRangeX(m_spectData.first().x(), m_spectData.last().x());
    ui->spectrum->showCallouts(false);

    fillList();

    connect(ui->savedList, &QListWidget::currentRowChanged, [=]() {
        ui->applySaved->setEnabled(true);
        ui->deleteSaved->setEnabled(true);
    });

    connect(ui->savedList, &QListWidget::itemDoubleClicked, [=](QListWidgetItem *item) {
        applySaved(ui->savedList->row(item));
    });
}

CalibrationDialog::~CalibrationDialog()
{
    delete ui;
}

void CalibrationDialog::on_toMakeButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void CalibrationDialog::on_toLoadButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void CalibrationDialog::on_selectFile_clicked()
{
    CsvFileDialog dialog("Load calibration certificate CSV", QFileDialog::AcceptOpen, this);
    if (dialog.exec() == QDialog::Accepted)
    {
        QString path = dialog.getFilePath();
        QChar textSeparator = dialog.getTextSepartaor();
        QChar fieldSeparator = dialog.getFieldsSepartaor();

        CsvHandler handler;
        handler.setTextSeparator(textSeparator);
        handler.setFieldSeparator(fieldSeparator);

        if (!handler.loadCsv(path))
        {
            QMessageBox::critical(this, "Import error!", "Cannot import this file!", QMessageBox::Ok, QMessageBox::NoButton);
            return;
        }

        bool ok = false;

        QVector<QPointF> data = handler.toDataVector(&ok);

        if (!ok)
        {
            QMessageBox::critical(this, "Import error!", "Cannot import this file!", QMessageBox::Ok, QMessageBox::NoButton);
            return;
        }

        m_calibCert = data;

        makeCalib(m_calibCert, m_spectData);

        ui->certPath->setText(path);
        ui->rb_specm->setChecked(true);
        ui->rb_file->setEnabled(true);
        ui->rb_specm->setEnabled(true);
        ui->applyNew->setEnabled(true);
    }
}

void CalibrationDialog::makeCalib(const QVector<QPointF> &cert, const QVector<QPointF> &measure)
{
    QubicSpline splinerCert(cert);
    QubicSpline splinerMeasure(measure);

    int size = m_spectData.size();

    QVector<qreal> wavelenghts(size);
    for (int i = 0; i < size; i++)
        wavelenghts[i] = m_spectData[i].x();

    QVector<QPointF> splinedCert = splinerCert.spline(wavelenghts);
    QVector<QPointF> splinedMeasure = splinerMeasure.spline(wavelenghts);

    qreal max = 0, max2 = 0;

    for (int i = 0; i < size; i++)
    {
        if (splinedCert[i].y() > max)
            max = splinedCert[i].y();

        if (splinedMeasure[i].y() > max2)
            max2 = splinedMeasure[i].y();
    }

    for (int i = 0; i < size; i++)
    {
        if (splinedCert[i].y() > 0)
            splinedCert[i].setY(splinedCert[i].y() / max);
        if (splinedMeasure[i].y() > 0)
        splinedMeasure[i].setY(splinedMeasure[i].y() / max2);
    }

    max = 0;
    for (int i = 0; i < size; i++)
    {
        m_calibCoeff[i] = QPointF(wavelenghts[i], splinedCert[i].y() / splinedMeasure[i].y());
        if (m_calibCoeff[i].y() > max)
            max = m_calibCoeff[i].y();
    }

    for (int i = 0; i < size; i++)
        if (m_calibCoeff[i].y() > 0)
            m_calibCoeff[i].setY(m_calibCoeff[i].y() / max);

    ui->spectrum->setData(m_calibCoeff);
}

void CalibrationDialog::fillList()
{
    m_calibList = CalibrationManager::getCalibrations();

    int size = m_calibList.size();

    if (size > 0)
    {
        ui->savedList->clear();

        for (int i = 0; i < size; i++)
            ui->savedList->addItem(m_calibList[i].second);
    }
}

void CalibrationDialog::on_rb_specm_toggled(bool checked)
{
    if (checked)
    {
        ui->samplePath->setText("spectrometer");
        makeCalib(m_calibCert, m_spectData);
    }
}

void CalibrationDialog::on_rb_file_toggled(bool checked)
{
    if (checked)
    {
        CsvFileDialog dialog("Load calibration sample CSV", QFileDialog::AcceptOpen, this);
        if (dialog.exec() == QDialog::Accepted)
        {
            QString path = dialog.getFilePath();
            QChar textSeparator = dialog.getTextSepartaor();
            QChar fieldSeparator = dialog.getFieldsSepartaor();

            CsvHandler handler;
            handler.setTextSeparator(textSeparator);
            handler.setFieldSeparator(fieldSeparator);

            if (!handler.loadCsv(path))
            {
                QMessageBox::critical(this, "Import error!", "Cannot import this file!", QMessageBox::Ok, QMessageBox::NoButton);
                ui->rb_specm->setChecked(true);
                return;
            }

            bool ok = false;

            QVector<QPointF> data = handler.toDataVector(&ok);

            if (!ok)
            {
                QMessageBox::critical(this, "Import error!", "Cannot import this file!", QMessageBox::Ok, QMessageBox::NoButton);
                ui->rb_specm->setChecked(true);
                return;
            }

            m_fileData = data;

            makeCalib(m_calibCert, m_fileData);

            ui->samplePath->setText(path);
        }
        else
        {
            ui->rb_specm->setChecked(true);
        }
    }
}

void CalibrationDialog::on_applyNew_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setText("Do you want to save calibration?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes)
    {
        SaveDialog dialog(this);
        dialog.setWindowTitle("Save calibration");

        if (dialog.exec() == QDialog::Accepted)
            CalibrationManager::insertCalibration(dialog.getName(), m_calibCoeff);
    }

    accept();
}

QVector<QPointF> CalibrationDialog::calibCoeff() const
{
    return m_calibCoeff;
}

void CalibrationDialog::on_deleteSaved_clicked()
{
    int row = ui->savedList->currentRow();

    int id = m_calibList[row].first;

    if (CalibrationManager::deleteCalibration(id))
    {
        QListWidgetItem *item = ui->savedList->takeItem(row);
        delete item;
        m_calibList.removeAt(row);
    }

    if (ui->savedList->count() == 0)
    {
        ui->applySaved->setEnabled(false);
        ui->deleteSaved->setEnabled(false);
    }
}

void CalibrationDialog::on_applySaved_clicked()
{
    applySaved(ui->savedList->currentRow());
}

void CalibrationDialog::applySaved(int row)
{
    QVector<QPointF> data = CalibrationManager::getCalibrationData(m_calibList[row].first);

    if (data.size() > 2)
    {
        int size = m_spectData.size();

        QVector<qreal> wavelenghts(size);
        for (int i = 0; i < size; i++)
            wavelenghts[i] = m_spectData[i].x();

        QubicSpline spliner(data);

        m_calibCoeff = spliner.spline(wavelenghts);

        qreal max = 0;

        for (int i = 0; i < m_calibCoeff.size(); i++)
            if (m_calibCoeff[i].y() > max)
                max = m_calibCoeff[i].y();

        for (int i = 0; i < m_calibCoeff.size(); i++)
        {
            if (m_calibCoeff[i].y() > 0)
                m_calibCoeff[i].setY(m_calibCoeff[i].y() / max);
        }
    }

    accept();
}
