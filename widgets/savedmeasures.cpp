#include "savedmeasures.h"
#include "ui_savedmeasures.h"
#include "items/measureitem.h"
#include "database/managers/measuremanager.h"
#include "measureview.h"
#include <QtDebug>

SavedMeasures::SavedMeasures(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SavedMeasures)
{
    ui->setupUi(this);

    updatePageNumbers();
    toPage(0);
}

SavedMeasures::~SavedMeasures()
{
    delete ui;
}

bool SavedMeasures::toPage(int page, bool update)
{
    if (page < 0 || (page == m_page && !update))
        return false;

    QList<MeasureRow> list = MeasureManager::getMeasures(page, 20);
    if (list.size() > 0)
    {
        ui->listWidget->clear();

        for (int i = 0; i < list.size(); i++)
        {
            QListWidgetItem *item = new QListWidgetItem("", ui->listWidget);
            MeasureItem *widget = new MeasureItem(list[i]);
            ui->listWidget->setItemWidget(item, widget);
            item->setSizeHint(widget->size());
        }

        m_data = list;
        m_page = page;

        updatePageNumbers();
        return true;
    }

    return false;
}

void SavedMeasures::setVisible(bool visible)
{
    QWidget::setVisible(visible);
}

void SavedMeasures::refresh()
{
    toPage(m_page, true);
}

void SavedMeasures::on_prevButton_clicked()
{
    toPage(m_page - 1);
}

void SavedMeasures::updatePageNumbers()
{
    int pages = MeasureManager::getMeasuresCount();
    if (pages > 0)
        pages = (pages / 20) + (((pages % 20) == 0) ? 0 : 1);

    ui->pagesLabel->setText(QString::number(pages));
    ui->pageSpin->setValue(m_page + 1);
}

void SavedMeasures::on_nextButton_clicked()
{
    toPage(m_page + 1);
}

void SavedMeasures::on_pageSpin_valueChanged(int arg1)
{
    toPage(arg1 - 1);
    updatePageNumbers();
}

void SavedMeasures::on_deleteButton_clicked()
{
    QListWidget *listWidget = ui->listWidget;

    if (listWidget->selectedItems().size() > 0)
    {
        if (MeasureManager::deleteMeasure(m_data[listWidget->currentRow()]))
        {
            if (m_data.size() > 1)
                toPage(m_page, true);
            else
                toPage(m_page - 1);
        }
    }
}

void SavedMeasures::on_openButton_clicked()
{
    QListWidget *listWidget = ui->listWidget;

    if (listWidget->selectedItems().size() > 0)
    {
        MeasureRow row = m_data[listWidget->currentRow()];
        QVector<QPointF> data = MeasureManager::getMeasureData(row);
        MeasureView *window = new MeasureView(data, row.name(), true);
        window->show();
    }
}

void SavedMeasures::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    int index = ui->listWidget->row(item);

    MeasureRow row = m_data[index];
    QVector<QPointF> data = MeasureManager::getMeasureData(row);
    MeasureView *window = new MeasureView(data, row.name());
    window->show();
}
