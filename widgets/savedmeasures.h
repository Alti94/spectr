#ifndef SAVEDMEASURES_H
#define SAVEDMEASURES_H

#include <QWidget>
#include <QListWidgetItem>
#include "database/rows/measurerow.h"

namespace Ui {
class SavedMeasures;
}

class SavedMeasures : public QWidget
{
    Q_OBJECT

public:
    explicit SavedMeasures(QWidget *parent = 0);
    ~SavedMeasures();

public slots:
    bool toPage(int page, bool update = false);
    void setVisible(bool visible);
    void refresh();

private slots:
    void on_prevButton_clicked();
    void on_nextButton_clicked();
    void on_pageSpin_valueChanged(int arg1);
    void on_deleteButton_clicked();
    void on_openButton_clicked();
    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::SavedMeasures *ui;
    QList<MeasureRow> m_data;
    int m_page = -1;

    void updatePageNumbers();
};

#endif // SAVEDMEASURES_H
