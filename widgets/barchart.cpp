#include "barchart.h"
#include <QtDebug>

BarChart::BarChart(QWidget *parent) :
    QChartView(parent),
    m_chart(new QChart()),
    m_dataSeries(new QBarSeries()),
    m_axisX(new QBarCategoryAxis()),
    m_axisY(new QCategoryAxis())
{
    m_chart->addSeries(m_dataSeries);

    m_axisY->setMax(100);
    m_axisY->setMin(0);
    m_axisY->setLabelsPosition(QCategoryAxis::AxisLabelsPositionOnValue);

    m_chart->setAxisX(m_axisX, m_dataSeries);
    m_chart->setAxisY(m_axisY, m_dataSeries);

    m_chart->legend()->hide();
    m_chart->setMargins(QMargins(0, 0, 0, 0));

    setChart(m_chart);
}

void BarChart::setXValues(QStringList values)
{
    m_axisX->clear();
    m_axisX->append(values);
}

void BarChart::setData(QVector<qreal> data)
{
    m_dataSeries->clear();

    QBarSet *set = new QBarSet("Data");
    set->append(data.toList());

    qreal min = 0;
    for (int i = 0; i < data.size(); i++)
        if (min > data[i])
            min = data[i];

    m_axisX->clear();
    QList<qreal> list;
    for (int i = 100; i >= min; i -= 20)
        list.append(i);

    for (QList<qreal>::iterator i = list.end(); i != list.begin(); i--)
        m_axisY->append(QString::number(*(i - 1)), *(i - 1));

    m_axisY->setMin(min);

    m_dataSeries->append(set);
}

void BarChart::setYAxisTitle(QString string)
{
    m_axisY->setTitleText(string);
}

void BarChart::setXAxisTitle(QString string)
{
    m_axisX->setTitleText(string);
}

void BarChart::setA(qreal value, QString title)
{
    if (m_dataSeries->barSets().size() > 0)
    {
        QBarSet *setIn = m_dataSeries->barSets().at(0);
        QBarSet *set = new QBarSet(title);
        for (int i = 0; i < setIn->count(); i++)
            set->append(0);

        set->append(value);
        m_dataSeries->append(set);

        m_axisX->append(title);
    }
}
