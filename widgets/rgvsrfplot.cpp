#include "rgvsrfplot.h"

RgVsRfPlot::RgVsRfPlot(QWidget *parent) :
    QChartView(parent),
    m_chart(new QChart),
    m_pointSeries(new QScatterSeries),
    m_axisX(new QValueAxis),
    m_axisY(new QValueAxis),
    size(QSize(500, 500)),
    m_posBox(0),
    m_text1(0),
    m_text2(0),
    m_text3(0),
    m_text4(0)
{
    m_chart->setMinimumSize(400, 400);
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    setBackgroundBrush(QBrush(Qt::white));

    initAxes();
    initAreas();
    initTexts();

    m_pointSeries->setMarkerShape(QScatterSeries::MarkerShapeRectangle);
    m_pointSeries->setMarkerSize(8);
    m_chart->addSeries(m_pointSeries);
    m_chart->setAxisX(m_axisX, m_pointSeries);
    m_chart->setAxisY(m_axisY, m_pointSeries);

    m_chart->legend()->hide();
    setChart(m_chart);

    connect(m_pointSeries, SIGNAL(hovered(QPointF,bool)), this, SLOT(showCoords(QPointF,bool)));
}

void RgVsRfPlot::setRgRf(qreal rg, qreal rf)
{
    m_pointSeries->append(rf, rg);
}

void RgVsRfPlot::showCoords(QPointF point, bool state)
{
    if (m_posBox == 0)
        m_posBox = new Callout(m_chart);

    if (state) {
        m_posBox->setText(QString("Rf: %1 \nRg: %2 ").arg(point.x(), 0, 'f', 2).arg(point.y(), 0, 'f', 2));
        m_posBox->setAnchor(point);
        m_posBox->setZValue(12);
        m_posBox->updateGeometry();
        m_posBox->show();
    } else {
        m_posBox->hide();
    }
}

void RgVsRfPlot::initAxes()
{
    m_axisX->setMin(50);
    m_axisX->setMax(100);
    m_axisX->setTickCount(6);
    m_axisX->setGridLineVisible(false);
    m_axisX->setTitleText("Rf");
    m_axisX->setTitleVisible();
    m_chart->addAxis(m_axisX, Qt::AlignBottom);

    m_axisY->setMin(60);
    m_axisY->setMax(140);
    m_axisY->setTickCount(9);
    m_axisY->setGridLineVisible(false);
    m_axisY->setTitleText("Rg");
    m_axisY->setTitleVisible();
    m_chart->addAxis(m_axisY, Qt::AlignLeft);
}

void RgVsRfPlot::initAreas()
{
    QLineSeries *up1 = new QLineSeries;
    up1->append(50, 140);
    up1->append(100, 140);

    QLineSeries *up2 = new QLineSeries;
    up2->append(50, 140);
    up2->append(100, 100);

    QLineSeries *up3 = new QLineSeries;
    up3->append(60, 140);
    up3->append(100, 100);

    QLineSeries *down1 = new QLineSeries;
    down1->append(50, 60);
    down1->append(100, 60);

    QLineSeries *down2 = new QLineSeries;
    down2->append(50, 60);
    down2->append(100, 100);

    QLineSeries *down3 = new QLineSeries;
    down3->append(60, 60);
    down3->append(100, 100);

    QAreaSeries *upper1 = new QAreaSeries(up1, up2);
    QAreaSeries *upper2 = new QAreaSeries(up1, up3);
    QAreaSeries *lower1 = new QAreaSeries(down2, down1);
    QAreaSeries *lower2 = new QAreaSeries(down3, down1);

    QPen pen1(0xf2f2f2);
    QPen pen2(0xbfbfbf);
    pen1.setWidth(1);
    pen2.setWidth(1);

    QBrush brush1(0xf2f2f2);
    QBrush brush2(0xbfbfbf);

    upper1->setPen(pen1);
    upper1->setBrush(brush1);
    upper2->setPen(pen2);
    upper2->setBrush(brush2);
    lower1->setPen(pen1);
    lower1->setBrush(brush1);
    lower2->setPen(pen2);
    lower2->setBrush(brush2);

    m_chart->addSeries(upper1);
    m_chart->addSeries(upper2);
    m_chart->addSeries(lower1);
    m_chart->addSeries(lower2);

    m_chart->setAxisX(m_axisX, upper1);
    m_chart->setAxisY(m_axisY, upper1);
    m_chart->setAxisX(m_axisX, upper2);
    m_chart->setAxisY(m_axisY, upper2);
    m_chart->setAxisX(m_axisX, lower1);
    m_chart->setAxisY(m_axisY, lower1);
    m_chart->setAxisX(m_axisX, lower2);
    m_chart->setAxisY(m_axisY, lower2);
}

void RgVsRfPlot::initTexts()
{
    m_text1 = new QGraphicsSimpleTextItem("(1)", m_chart);
    m_text1->setZValue(11);
    m_text2 = new QGraphicsSimpleTextItem("(2)", m_chart);
    m_text2->setZValue(11);
    m_text3 = new QGraphicsSimpleTextItem("(1) Approx. limits for \nsources on the \nPlanckian locus.", m_chart);
    m_text3->setZValue(11);
    m_text4 = new QGraphicsSimpleTextItem("(2) Approx. limits for \npractical light \nsources.", m_chart);
    m_text4->setZValue(11);
}

QSize RgVsRfPlot::sizeHint() const
{
    return size;
}

void RgVsRfPlot::resizeEvent(QResizeEvent *event)
{
    QChartView::resizeEvent(event);

    if (m_text1 != 0)
    {
        m_text1->setPos(m_chart->mapToPosition(QPointF(50, 140), m_pointSeries));

        m_text2->setPos(m_chart->mapToPosition(QPointF(60, 140), m_pointSeries));

        QPointF pos3 = m_chart->mapToPosition(QPointF(100, 140), m_pointSeries);
        pos3.setX(pos3.x() - m_text3->boundingRect().width());
        m_text3->setPos(pos3);

        QPointF pos4 = pos3;
        pos4.setY(pos4.y() + m_text3->boundingRect().height() + 20);
        m_text4->setPos(pos4);
    }
}
