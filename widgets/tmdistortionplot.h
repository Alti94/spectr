#ifndef TMDISTORTIONPLOT_H
#define TMDISTORTIONPLOT_H

#include "labchart.h"
#include "items/arrowgraphicsitem.h"

class TmDistortionPlot : public LabChart
{
    Q_OBJECT
public:
    explicit TmDistortionPlot(const QVector<QPointF> &ref, const QVector<QPointF> &test, QWidget *parent = nullptr);

private:
    QLineSeries *m_referenceDistortion;
    QLineSeries *m_testDistortion;
    QList<ArrowGraphicsItem*> m_arrows;

    QGraphicsTextItem *m_legendTestLabel;
    QGraphicsLineItem *m_legendTestLine;
    QGraphicsTextItem *m_legendRefLabel;
    QGraphicsLineItem *m_legendRefLine;

    void initArrows();
    void initLegend();

protected:
    void resizeEvent(QResizeEvent *event);
};

#endif // TMDISTORTIONPLOT_H
