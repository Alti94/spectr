#include "customfiledialog.h"

CustomFileDialog::CustomFileDialog(QWidget *parent) : QFileDialog(parent)
{
    setSizeGripEnabled(false);
}

void CustomFileDialog::setPermitToAccept(bool permitToAccept)
{
    m_permitToAccept = permitToAccept;
}

void CustomFileDialog::setVisible(bool visible)
{
    QFileDialog::setVisible(visible);
    setSizeGripEnabled(false);
}

void CustomFileDialog::accept()
{
    if (m_permitToAccept)
        QFileDialog::accept();
}
