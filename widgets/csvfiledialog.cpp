#include "csvfiledialog.h"
#include "ui_csvfiledialog.h"

CsvFileDialog::CsvFileDialog(const QString &windowTitle, QFileDialog::AcceptMode mode, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CsvFileDialog),
    m_fileDialog(new CustomFileDialog)
{
    ui->setupUi(this);
    ui->container->addWidget(m_fileDialog);
    setSizeGripEnabled(true);

    setWindowTitle(windowTitle);
    m_fileDialog->setAcceptMode(mode);

    QStringList filters;
    filters << "CSV file (*.csv)"
            << "Any files (*)";

    m_fileDialog->setNameFilters(filters);


    connect(m_fileDialog, SIGNAL(fileSelected(QString)), this, SLOT(openFileSlot(QString)));
    connect(m_fileDialog, SIGNAL(rejected()), this, SLOT(reject()));
}

void CsvFileDialog::setAcceptMode(QFileDialog::AcceptMode mode)
{
    m_fileDialog->setAcceptMode(mode);
}

QString CsvFileDialog::getFilePath() const
{
    if (m_fileDialog->selectedFiles().size() > 0)
        return m_fileDialog->selectedFiles().at(0);
    else
        return "";
}

CsvFileDialog::~CsvFileDialog()
{
    delete ui;
}

QChar CsvFileDialog::getTextSepartaor() const
{
    if (ui->textSeparator->text().size() > 0)
        return ui->textSeparator->text()[0];
    else
        return '"';
}

QChar CsvFileDialog::getFieldsSepartaor() const
{
    if (ui->fieldSeparator->text().size() > 0)
        return ui->fieldSeparator->text()[0];
    else
        return ';';
}

void CsvFileDialog::openFileSlot(const QString &path)
{
    emit csvSelected(path, ui->fieldSeparator->text()[0], ui->fieldSeparator->text()[0]);
    accept();
}

void CsvFileDialog::fieldsFilled()
{
    if (ui->fieldSeparator->text().size() == 1 && ui->textSeparator->text().size() == 1)
        m_fileDialog->setPermitToAccept(true);
    else
        m_fileDialog->setPermitToAccept(false);
}
