#ifndef CQSSAMPLESCOLORS_H
#define CQSSAMPLESCOLORS_H

#include <QWidget>
#include "../colorimetry/colorimetric_structs.h"

namespace Ui {
class CqsSamplesColors;
}

class CqsSamplesColors : public QWidget
{
    Q_OBJECT

public:
    explicit CqsSamplesColors(QWidget *parent = 0);
    void setReference(const QVector<CIE_XYZ> &samples, const CIE_XYZ light);
    void setTest(const QVector<CIE_XYZ> &samples, const CIE_XYZ light);
    ~CqsSamplesColors();

private:
    Ui::CqsSamplesColors *ui;
};

#endif // CQSSAMPLESCOLORS_H
