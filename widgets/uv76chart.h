#ifndef UV76CHART_H
#define UV76CHART_H

#include <QtCharts>
#include "callout.h"

QT_CHARTS_USE_NAMESPACE

class Uv76Chart : public QChartView
{
    Q_OBJECT
public:
    explicit Uv76Chart(QWidget *parent = nullptr);
    ~Uv76Chart();
    void setPoint(const QPointF &point);

public slots:
    void showGrid(bool grid = true);
    void fillColor(bool color = true);
    void showBbLocus(bool bbLocus = true);

private:
    struct TempMark
    {
        QString temp;
        QPointF start;
        QPointF end;
    };

    static const QVector<QPointF> m_uv76chroma;
    static const QVector<QPointF> m_blackBodyLocus;
    static const QVector<TempMark> m_tempMarks;
    QLineSeries *m_lineSeries;
    QScatterSeries *m_uv76Point;
    QSplineSeries *m_blackBodySeries;
    QChart *m_chart;
    QPixmap m_bgPixmap;
    Callout *m_posBox;
    QVector<QGraphicsLineItem*> m_markLines;
    QVector<QGraphicsTextItem*> m_markLabels;

    void initMarks();
    void resizeMarks();

protected:
    void resizeEvent(QResizeEvent *event);

private slots:
    void showCoords(QPointF point, bool state);
};

#endif // UV76CHART_H
