#ifndef CQSICON_H
#define CQSICON_H

#include <QtCharts>

QT_CHARTS_USE_NAMESPACE

class CqsIcon : public QChartView
{
    Q_OBJECT
public:
    enum IconType {
        IconA,
        IconB
    };

    explicit CqsIcon(IconType type = IconA, QWidget *parent = nullptr);
    void setData(const QVector<qreal> &data);

private:
    QPolarChart *m_chart;
    QLineSeries *m_dataSeries;
    QValueAxis *m_radialAxis;
    QValueAxis *m_angularAxis;
    QPixmap m_background;

protected:
    void resizeEvent(QResizeEvent *event);
};

#endif // CQSICON_H
