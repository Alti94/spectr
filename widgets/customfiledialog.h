#ifndef QCUSTOMFD_H
#define QCUSTOMFD_H

#include <QFileDialog>

class CustomFileDialog : public QFileDialog
{
    Q_OBJECT
public:
    explicit CustomFileDialog(QWidget *parent = nullptr);

    void setPermitToAccept(bool permitToAccept);

private:
    bool m_permitToAccept = true;

public slots:
    void setVisible(bool visible);
    void accept();
};

#endif // QCUSTOMFD_H
