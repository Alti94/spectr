#ifndef MEASUREITEM_H
#define MEASUREITEM_H

#include <QWidget>
#include "database/rows/measurerow.h"

namespace Ui {
class MeasureItem;
}

class MeasureItem : public QWidget
{
    Q_OBJECT

public:
    explicit MeasureItem(const MeasureRow &row, QWidget *parent = 0);
    ~MeasureItem();

    MeasureRow getRow() const;
    void setRow(const MeasureRow &value);

private:
    Ui::MeasureItem *ui;
    MeasureRow row;
};

#endif // MEASUREITEM_H
