#include "arrowgraphicsitem.h"
#include <QtDebug>
#include <QPainter>

ArrowGraphicsItem::ArrowGraphicsItem(const QLineF &line, QGraphicsItem *item) :
    QGraphicsLineItem(line, item)
{
    setArrow();
}

void ArrowGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QGraphicsLineItem::paint(painter, option, widget);
    painter->drawLine(m_arrowHead1);
    painter->drawLine(m_arrowHead2);
}

void ArrowGraphicsItem::setLine(const QLineF &line)
{
    QGraphicsLineItem::setLine(line);
    setArrow();
}

void ArrowGraphicsItem::setLine(qreal x1, qreal y1, qreal x2, qreal y2)
{
    QGraphicsLineItem::setLine(x1, y1, x2, y2);
    setArrow();
}

QRectF ArrowGraphicsItem::boundingRect() const
{
    return m_boundingRect;
}

void ArrowGraphicsItem::setArrowSize(const qreal &arrowSize)
{
    m_arrowSize = arrowSize;
}

void ArrowGraphicsItem::setArrow()
{
    QPointF start = line().p2();
    QPointF end = start;
    end.setX(end.x() + line().length() * m_arrowSize);
    m_arrowHead1 = QLineF(start, end);
    m_arrowHead2 = QLineF(start, end);

    qreal lineAngle = line().angle();

    m_arrowHead1.setAngle(135 + lineAngle);
    m_arrowHead2.setAngle(-135 + lineAngle);

    calculateBoundRect();
}

void ArrowGraphicsItem::calculateBoundRect()
{
    m_boundingRect = QGraphicsLineItem::boundingRect();

    QPointF point = m_arrowHead1.p2();

    if (m_boundingRect.top() > point.y())
        m_boundingRect.setTop(point.y());

    if (m_boundingRect.bottom() < point.y())
        m_boundingRect.setBottom(point.y());

    if (m_boundingRect.left() > point.x())
        m_boundingRect.setLeft(point.x());

    if (m_boundingRect.right() < point.x())
        m_boundingRect.setRight(point.x());

    point = m_arrowHead2.p2();

    if (m_boundingRect.top() > point.y())
        m_boundingRect.setTop(point.y());

    if (m_boundingRect.bottom() < point.y())
        m_boundingRect.setBottom(point.y());

    if (m_boundingRect.left() > point.x())
        m_boundingRect.setLeft(point.x());

    if (m_boundingRect.right() < point.x())
        m_boundingRect.setRight(point.x());
}
