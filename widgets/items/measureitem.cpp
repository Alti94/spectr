#include "measureitem.h"
#include "ui_measureitem.h"

MeasureItem::MeasureItem(const MeasureRow &row, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MeasureItem)
{
    ui->setupUi(this);
    ui->spectrum->setScene(new QGraphicsScene(0, 0, 200, 80));
    setRow(row);
}

MeasureItem::~MeasureItem()
{
    delete ui;
}

MeasureRow MeasureItem::getRow() const
{
    return row;
}

void MeasureItem::setRow(const MeasureRow &value)
{
    row = value;

    ui->name->setText(row.name());
    ui->date->setText(row.date().toString());
    ui->spectrum->scene()->addPixmap(row.miniature());
}
