#ifndef ARROWGRAPHICSITEM_H
#define ARROWGRAPHICSITEM_H

#include <QGraphicsLineItem>

class ArrowGraphicsItem : public QGraphicsLineItem
{
public:
    ArrowGraphicsItem(const QLineF &line, QGraphicsItem *item = nullptr);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void setLine(const QLineF &line);
    void setLine(qreal x1, qreal y1, qreal x2, qreal y2);
    QRectF boundingRect() const;
    void setArrowSize(const qreal &arrowSize);

private:
    QLineF m_arrowHead1;
    QLineF m_arrowHead2;
    QRectF m_boundingRect;
    qreal m_arrowSize = 0.2;

    void setArrow();
    void calculateBoundRect();
};

#endif // ARROWGRAPHICSITEM_H
