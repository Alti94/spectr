#include "cqsicon.h"

#include <QtDebug>

CqsIcon::CqsIcon(IconType type, QWidget *parent) :
    QChartView(parent),
    m_chart(new QPolarChart),
    m_dataSeries(new QLineSeries),
    m_radialAxis(new QValueAxis),
    m_angularAxis(new QValueAxis),
    m_background(QPixmap(":/img/icon.png", "PNG"))
{
    m_chart->setMinimumSize(300, 300);
    m_chart->setMargins(QMargins(0, 0, 0, 0));
    setBackgroundBrush(QBrush(Qt::white));

    m_angularAxis->setTickCount(16);
    m_angularAxis->setRange(1, 16);
    m_angularAxis->setLabelsVisible(false);
    m_chart->addAxis(m_angularAxis, QPolarChart::PolarOrientationAngular);

    if (type == IconA)
    {
        m_radialAxis->setTickCount(6);
        m_radialAxis->setRange(0, 100);
    }
    else
    {
        m_radialAxis->setTickCount(7);
        m_radialAxis->setRange(0, 120);

        QSplineSeries *series100 = new QSplineSeries;
        for (int i = 1; i <= 16; i++)
            series100->append(i, 100);

        QPen pen(Qt::blue);
        pen.setWidth(2);
        pen.setStyle(Qt::SolidLine);
        series100->setPen(pen);

        m_chart->addSeries(series100);
        m_chart->setAxisX(m_angularAxis, series100);
        m_chart->setAxisY(m_radialAxis, series100);
    }

    m_chart->addAxis(m_radialAxis, QPolarChart::PolarOrientationRadial);

    QPen pen(Qt::black);
    pen.setWidth(3);
    pen.setStyle(Qt::SolidLine);
    m_dataSeries->setPen(pen);

    m_chart->addSeries(m_dataSeries);
    m_chart->setAxisX(m_angularAxis, m_dataSeries);
    m_chart->setAxisY(m_radialAxis, m_dataSeries);

    m_chart->legend()->hide();

    QBrush brush;
    brush.setTexture(m_background);
    m_chart->setPlotAreaBackgroundBrush(brush);
    m_chart->setPlotAreaBackgroundVisible(true);

    setChart(m_chart);
}

void CqsIcon::setData(const QVector<qreal> &data)
{
    for (int i = 0; i < 15; i++)
        m_dataSeries->append(i + 1, data[(i + 9) % 15]);

    m_dataSeries->append(16, data[9]);
}

void CqsIcon::resizeEvent(QResizeEvent *event)
{
    QChartView::resizeEvent(event);

    QRectF rect = m_chart->plotArea();

    QTransform transform;
    transform.translate(rect.x(), rect.y());
    transform.scale(rect.width() / m_background.width(), rect.height() / m_background.height());

    QBrush brush = m_chart->plotAreaBackgroundBrush();
    brush.setTransform(transform);
    m_chart->setPlotAreaBackgroundBrush(brush);
}
