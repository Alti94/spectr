#ifndef CALIBRATIONDIALOG_H
#define CALIBRATIONDIALOG_H

#include <QDialog>

namespace Ui {
class CalibrationDialog;
}

class CalibrationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CalibrationDialog(const QVector<QPointF> &data, QWidget *parent = 0);
    ~CalibrationDialog();

    QVector<QPointF> calibCoeff() const;

private slots:
    void on_toMakeButton_clicked();
    void on_toLoadButton_clicked();
    void on_selectFile_clicked();
    void on_rb_specm_toggled(bool checked);
    void on_rb_file_toggled(bool checked);
    void on_applyNew_clicked();
    void on_deleteSaved_clicked();
    void on_applySaved_clicked();

    void applySaved(int row);

private:
    Ui::CalibrationDialog *ui;
    QVector<QPointF> m_spectData;
    QVector<QPointF> m_calibCoeff;
    QVector<QPointF> m_calibCert;
    QVector<QPointF> m_fileData;

    QList<QPair<int, QString>> m_calibList;


    void makeCalib(const QVector<QPointF> &cert, const QVector<QPointF> &measure);
    void fillList();
};

#endif // CALIBRATIONDIALOG_H
