#include "tmdistortionplot.h"

TmDistortionPlot::TmDistortionPlot(const QVector<QPointF> &ref, const QVector<QPointF> &test, QWidget *parent) :
    LabChart(parent),
    m_referenceDistortion(new QLineSeries),
    m_testDistortion(new QLineSeries)
{
    for (int i = 0; i < 16; i++)
    {
        m_referenceDistortion->append(ref[i] * 40);
        m_testDistortion->append(test[i] * 40);
    }
    m_referenceDistortion->append(m_referenceDistortion->at(0));
    m_testDistortion->append(m_testDistortion->at(0));

    QPen pen = m_referenceDistortion->pen();
    pen.setColor(QColor(0, 0, 255));
    pen.setWidth(2);
    m_referenceDistortion->setPen(pen);

    pen = m_testDistortion->pen();
    pen.setWidth(2);
    pen.setColor(QColor(255, 0, 0));
    m_testDistortion->setPen(pen);

    m_chart->setMargins(QMargins(0, 0, 0, 0));
    setBackgroundBrush(QBrush(Qt::white));

    m_axisX->setRange(-60, 60);
    m_axisY->setRange(-60, 60);
    m_axisX->setTickCount(7);
    m_axisY->setTickCount(7);
    m_axisX->setLabelsVisible(false);
    m_axisY->setLabelsVisible(false);
    m_axisX->setLineVisible(false);
    m_axisY->setLineVisible(false);
    m_axisX->setTitleVisible(false);
    m_axisY->setTitleVisible(false);

    m_chart->addSeries(m_testDistortion);
    m_chart->setAxisX(m_axisX, m_testDistortion);
    m_chart->setAxisY(m_axisY, m_testDistortion);

    m_chart->addSeries(m_referenceDistortion);
    m_chart->setAxisX(m_axisX, m_referenceDistortion);
    m_chart->setAxisY(m_axisY, m_referenceDistortion);

    initArrows();
    initLegend();

    m_chart->setMinimumSize(400, 400);
}

void TmDistortionPlot::initArrows()
{
    for (int i = 0; i < 16; i++)
    {
        QPointF start = m_chart->mapToPosition(m_referenceDistortion->at(i), m_referenceDistortion);
        QPointF end = m_chart->mapToPosition(m_testDistortion->at(i), m_testDistortion);
        QLineF line(start, end);
        ArrowGraphicsItem *arrow = new ArrowGraphicsItem(line, m_chart);
        arrow->setZValue(10);
        m_arrows += arrow;
    }
}

void TmDistortionPlot::initLegend()
{
    QLineF line(0, 0, 25, 0);
    m_legendRefLine = new QGraphicsLineItem(line, m_chart);
    m_legendTestLine = new QGraphicsLineItem(line, m_chart);
    m_legendRefLine->setZValue(15);
    m_legendTestLine->setZValue(15);

    m_legendRefLabel = new QGraphicsTextItem("Ref.", m_chart);
    m_legendTestLabel = new QGraphicsTextItem("Test", m_chart);
    m_legendRefLabel->setZValue(15);
    m_legendTestLabel->setZValue(15);

    QPen pen;
    pen.setWidth(2);
    pen.setColor(QColor(255, 0, 0));

    m_legendTestLine->setPen(pen);
    m_legendTestLabel->setDefaultTextColor(QColor(255, 0, 0));

    pen.setColor(QColor(0, 0, 255));
    m_legendRefLine->setPen(pen);
    m_legendRefLabel->setDefaultTextColor(QColor(0, 0, 255));
}

void TmDistortionPlot::resizeEvent(QResizeEvent *event)
{
    LabChart::resizeEvent(event);

    for (int i = 0; i < 16; i++)
    {
        QPointF start = m_chart->mapToPosition(m_referenceDistortion->at(i), m_referenceDistortion);
        QPointF end = m_chart->mapToPosition(m_testDistortion->at(i), m_testDistortion);
        QLineF line(start, end);
        m_arrows[i]->setLine(line);
    }

    QRectF area = m_chart->plotArea();
    QRectF testLabelBound = m_legendTestLabel->boundingRect();
    QRectF refLabelBound = m_legendRefLabel->boundingRect();

    m_legendTestLine->setPos(area.topLeft() + QPointF(0, testLabelBound.height() / 2));
    m_legendRefLine->setPos(area.topLeft() + QPointF(0, testLabelBound.height() + (refLabelBound.height() / 2)));

    m_legendTestLabel->setPos(area.topLeft() + QPointF(28, 0));
    m_legendRefLabel->setPos(area.topLeft() + QPointF(28, 0) + QPointF(0, testLabelBound.height()));
}
