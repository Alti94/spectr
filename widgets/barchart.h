#ifndef BARCHART_H
#define BARCHART_H

#include <QtCharts>

QT_CHARTS_USE_NAMESPACE

class BarChart : public QChartView
{
    Q_OBJECT

public:
    explicit BarChart(QWidget *parent = nullptr);
    void setXValues(QStringList values);
    void setData(QVector<qreal> data);
    void setYAxisTitle(QString string);
    void setXAxisTitle(QString string);
    void setA(qreal value, QString title);

private:
    QChart *m_chart;
    QBarSeries *m_dataSeries;
    QBarCategoryAxis *m_axisX;
    QCategoryAxis *m_axisY;
};

#endif // BARCHART_H
