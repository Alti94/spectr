#ifndef CRISAMPLESCOLORS_H
#define CRISAMPLESCOLORS_H

#include <QWidget>
#include "../colorimetry/colorimetric_structs.h"

namespace Ui {
class CriSamplesColors;
}

class CriSamplesColors : public QWidget
{
    Q_OBJECT

public:
    explicit CriSamplesColors(QWidget *parent = 0);
    void setReference(const QVector<CIE_XYZ> &samples, const CIE_XYZ light);
    void setTest(const QVector<CIE_XYZ> &samples, const CIE_XYZ light);
    ~CriSamplesColors();

private:
    Ui::CriSamplesColors *ui;
};

#endif // CRISAMPLESCOLORS_H
