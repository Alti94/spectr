#ifndef RGVSRFPLOT_H
#define RGVSRFPLOT_H

#include <QtCharts>
#include "callout.h"

QT_CHARTS_USE_NAMESPACE

class RgVsRfPlot : public QChartView
{
    Q_OBJECT
public:
    explicit RgVsRfPlot(QWidget *parent = nullptr);
    void setRgRf(qreal rg, qreal rf);

private slots:
    void showCoords(QPointF point, bool state);

private:
    QChart *m_chart;
    QScatterSeries *m_pointSeries;
    QValueAxis *m_axisX;
    QValueAxis *m_axisY;
    QSize size;
    Callout *m_posBox;
    QGraphicsSimpleTextItem *m_text1;
    QGraphicsSimpleTextItem *m_text2;
    QGraphicsSimpleTextItem *m_text3;
    QGraphicsSimpleTextItem *m_text4;

    void initAxes();
    void initAreas();
    void initTexts();

protected:
    QSize sizeHint() const;
    void resizeEvent(QResizeEvent *event);
};

#endif // RGVSRFPLOT_H
