#include "labchart.h"
#include "items/arrowgraphicsitem.h"
#include "../colorimetry/rgbconverter.h"

#include <QtDebug>

LabChart::LabChart(QWidget *parent) :
    QChartView(parent),
    m_placeholder(new QLineSeries),
    m_l(100),
    m_chart(new QChart),
    m_labColors(QPixmap(801, 801)),
    m_axisX(new QValueAxis),
    m_axisY(new QValueAxis)
{
    m_labColors.fill();
    generateLabColorSpace();

    m_axisX->setRange(-200, 200);
    m_axisX->setTitleText("a");
    m_axisY->setRange(-200, 200);
    m_axisY->setTitleText("b");

    m_chart->addAxis(m_axisX, Qt::AlignBottom);
    m_chart->addAxis(m_axisY, Qt::AlignLeft);

    m_chart->addSeries(m_placeholder);
    m_chart->setAxisX(m_axisX, m_placeholder);
    m_chart->setAxisY(m_axisY, m_placeholder);

    QBrush labBrush;
    labBrush.setTexture(m_labColors);
    m_chart->setPlotAreaBackgroundBrush(labBrush);
    m_chart->setPlotAreaBackgroundVisible();

    m_chart->setMinimumSize(400, 400);
    m_chart->legend()->hide();

    setChart(m_chart);
}

void LabChart::showBackground(bool background)
{
    m_chart->setPlotAreaBackgroundVisible(background);
}

void LabChart::setLabLValue(const qreal l)
{
    m_l = l;
    generateLabColorSpace();
    QBrush brush = m_chart->plotAreaBackgroundBrush();
    brush.setTexture(m_labColors);
    m_chart->setPlotAreaBackgroundBrush(brush);
}

void LabChart::resizeLabBg()
{
    QPointF topLeft = m_chart->mapToPosition(QPointF(-200, 200), m_placeholder);
    QPointF bottomRight = m_chart->mapToPosition(QPointF(200, -200), m_placeholder);

    QRectF rect;
    rect.setTopLeft(topLeft);
    rect.setBottomRight(bottomRight);

    QPointF offset = topLeft;
    QBrush brush = m_chart->plotAreaBackgroundBrush();
    QTransform transform;
    transform.translate(offset.x(), offset.y());
    transform.scale(rect.width() / m_labColors.width(), rect.height() / m_labColors.height());
    brush.setTransform(transform);

    m_chart->setPlotAreaBackgroundBrush(brush);
}

void LabChart::generateLabColorSpace()
{
    QImage image(801, 801, QImage::Format_RGB32);

    for (int row = 0; row < 801; row++)
    {
        for (int col = 0; col < 801; col++)
        {
            CieLAB lab = { m_l, (qreal) (col - 400) / 2, (qreal) (400 - row) / 2 };
            image.setPixelColor(col, row, RgbConverter::LabToRgb255(lab));
        }
    }

    m_labColors = QPixmap::fromImage(image);
}

void LabChart::resizeEvent(QResizeEvent *event)
{
    QChartView::resizeEvent(event);
    resizeLabBg();
}
