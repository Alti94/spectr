#ifndef LABCHART_H
#define LABCHART_H

#include <QtCharts>

QT_CHARTS_USE_NAMESPACE

class LabChart : public QChartView
{
    Q_OBJECT
public:
    explicit LabChart(QWidget *parent = nullptr);
    void showBackground(bool background = true);
    void setLabLValue(const qreal l);

signals:

public slots:

private:
    QLineSeries *m_placeholder;
    qreal m_l;

    void resizeLabBg();
    void generateLabColorSpace();

protected:
    QChart *m_chart;
    QPixmap m_labColors;
    QValueAxis *m_axisX;
    QValueAxis *m_axisY;

    void resizeEvent(QResizeEvent *event);
};

#endif // LABCHART_H
