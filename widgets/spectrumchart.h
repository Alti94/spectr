#ifndef SPECTRUMCHART_H
#define SPECTRUMCHART_H

#include <QtCharts>
#include "callout.h"

QT_CHARTS_USE_NAMESPACE

class SpectrumChart : public QChartView
{
    Q_OBJECT
public:
    explicit SpectrumChart(QWidget *parent = nullptr);
    void setData(QVector<QPointF> &data);
    void setRangeX(qreal min, qreal max, int ticks = 10);
    void setMaxY(qreal max, int ticks = 10);
    void showCallouts(bool callouts = true);
    QVector<QPointF> data() const;
    qreal maxY() const;

    QChart *chart() const;
    QValueAxis *axisX() const;
    QValueAxis *axisY() const;

public slots:
    void showSpectralBg(bool spectralBg = true);

private:
    QChart *m_chart;
    QLineSeries *m_dataSeries;
    QAreaSeries *m_dataArea;
    QValueAxis *m_axisX;
    QValueAxis *m_axisY;
    QPixmap m_spectralPixmap;
    Callout *m_callout;
    bool m_showCallouts;

protected:
    void resizeEvent(QResizeEvent *event);

signals:

private slots:
    void showCoords(QPointF point, bool state);
};

#endif // SPECTRUMCHART_H
