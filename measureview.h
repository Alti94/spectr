#ifndef MEASUREVIEW_H
#define MEASUREVIEW_H

#include <QMainWindow>
#include "colorimetry/chromacity.h"
#include "colorimetry/ciecri.h"
#include "colorimetry/cqs.h"
#include "colorimetry/tm3015.h"

namespace Ui {
class MeasureView;
}

class MeasureView : public QMainWindow
{
    Q_OBJECT

public:
    explicit MeasureView(const QVector<QPointF> &data, const QString &name = "New measure", bool saved = false, QWidget *parent = 0);
    ~MeasureView();

    QString getMeasureName() const;
    void setMeasureName(const QString &value);

private slots:
    void exportCsvRaw(const QString &path, const QChar &textSeparator, const QChar &fieldSeparator);
    void exportCsv(const QString &path, const QChar &textSeparator, const QChar &fieldSeparator);

    void on_rfVsRg_clicked();
    void on_tmDistortion_clicked();
    void on_cqsIconA_clicked();
    void on_cqsIconB_clicked();
    void on_actionCSV_export_raw_triggered();
    void on_actionCSV_export_triggered();
    void on_actionSave_triggered();

private:
    Ui::MeasureView *ui;
    Chromacity m_chroma;
    CieCRI m_cri;
    Cqs m_cqs;
    Tm3015 m_tm30;
    QVector<QPointF> m_rawData;
    QVector<QPointF> m_splinedData;
    QString m_measureName;
    bool m_saved;

    // Okna
    QWidget *m_iconAWindow, *m_iconBWindow,
            *m_rfrgWindow, *m_distortionWindow;


    void normalizeData();
    void initUv76Chart();
    void initCriTab(CieCRI &cri);
    void initCqsTab(Cqs &cqs);
    void initTm3015(Tm3015 &tm3015);
    void initWindows();

protected:
    void closeEvent(QCloseEvent *event);

};

#endif // MEASUREVIEW_H
