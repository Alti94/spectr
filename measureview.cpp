#include "measureview.h"
#include "ui_measureview.h"

#include "colorimetry/samplesdata.h"
#include "colorimetry/qubicspline.h"

#include "widgets/proportionalwidget.h"
#include "widgets/uv76chart.h"
#include "widgets/rgvsrfplot.h"
#include "widgets/tmdistortionplot.h"
#include "widgets/cqsicon.h"
#include "widgets/csvfiledialog.h"

#include "csvhandler.h"

#include "database/managers/measuremanager.h"
#include "database/rows/measurerow.h"

#include "widgets/savedmeasures.h"
#include "widgets/savedialog.h"

MeasureView::MeasureView(const QVector<QPointF> &data, const QString &name, bool saved, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MeasureView),
    m_chroma(Chromacity(data)),
    m_rawData(data),
    m_saved(saved)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    QubicSpline qubic(data);
    m_splinedData = qubic.spline(360, 830, 5);

    normalizeData();

    m_cri = CieCRI(m_chroma);
    m_cqs = Cqs(m_chroma);
    m_tm30 = Tm3015(m_chroma);

    ui->spectrum->setData(m_splinedData);
    ui->spectrum->setBackgroundBrush(Qt::white);
    ui->spectrum->setMaxY(1);

    initUv76Chart();
    initCriTab(m_cri);
    initCqsTab(m_cqs);
    initTm3015(m_tm30);
    initWindows();

    setMeasureName(name);

    connect(ui->cb_fillWave, SIGNAL(clicked(bool)), ui->spectrum, SLOT(showSpectralBg(bool)));
}

MeasureView::~MeasureView()
{
    delete m_iconAWindow;
    delete m_iconBWindow;
    delete m_rfrgWindow;
    delete m_distortionWindow;

    delete ui;
}

void MeasureView::exportCsvRaw(const QString &path, const QChar &textSeparator, const QChar &fieldSeparator)
{
    CsvHandler handler;
    handler.setFieldSeparator(fieldSeparator);
    handler.setTextSeparator(textSeparator);
    handler.toCsv(m_rawData);

    if (!handler.saveCsv(path))
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("Export error!");
        msgBox.setInformativeText("Cannot save file!");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
    }
}

void MeasureView::exportCsv(const QString &path, const QChar &textSeparator, const QChar &fieldSeparator)
{
    CsvHandler handler;
    handler.setFieldSeparator(fieldSeparator);
    handler.setTextSeparator(textSeparator);
    handler.toCsv(m_splinedData);

    if (!handler.saveCsv(path))
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("Export error!");
        msgBox.setInformativeText("Cannot save file!");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
    }
}

void MeasureView::initUv76Chart()
{
    Uv76Chart *chart = new Uv76Chart();
    chart->setBackgroundBrush(QBrush(Qt::white));

    QPointF point(m_chroma.uv76Chroma().uP, m_chroma.uv76Chroma().vP);
    chart->setPoint(point);

    ui->uvContainer->setWidget(chart);

    qreal temp = m_chroma.temperature().temp();
    QString temperature, duv;
    if (temp <= 1000)
    {
        temperature = "<= 1000 K";
        duv = "";
    }
    else if ( temp >= 20000)
    {
        temperature = ">= 20000 K";
        duv = "";
    }
    else
    {
        temperature = QString::number(m_chroma.temperature().temp(), 'f', 1) + " K";
        duv = QString::number(m_chroma.temperature().duv(), 'f', 4);
    }

    ui->le_cct->setText(temperature);
    ui->le_duv->setText(duv);

    ui->le_x->setText(QString::number(m_chroma.xyzChroma().x, 'f', 6));
    ui->le_y->setText(QString::number(m_chroma.xyzChroma().y, 'f', 6));

    ui->le_up->setText(QString::number(m_chroma.uv76Chroma().uP, 'f', 6));
    ui->le_vp->setText(QString::number(m_chroma.uv76Chroma().vP, 'f', 6));

    connect(ui->cb_showGrid, SIGNAL(clicked(bool)), chart, SLOT(showGrid(bool)));
    connect(ui->cb_showBb, SIGNAL(clicked(bool)), chart, SLOT(showBbLocus(bool)));
    connect(ui->cb_fillColor, SIGNAL(clicked(bool)), chart, SLOT(fillColor(bool)));
}

void MeasureView::initCriTab(CieCRI &cri)
{
    ui->criSamples->setReference(cri.refSamXYZ(), cri.referenceChroma().xyz());
    ui->criSamples->setTest(cri.testSamXYZ(), cri.testChroma().xyz());

    ui->criBarChart->setData(cri.ri());
    ui->criBarChart->setXValues(QStringList({ "R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8", "R9", "R10", "R11", "R12", "R13", "R14" }));
    ui->criBarChart->setYAxisTitle("Ri");
    ui->criBarChart->setA(cri.ra(), "Ra");

    QVector<qreal> ri = cri.ri();

    ui->ra->setText(QString::number(cri.ra(), 'f', 2));
    ui->r1->setText(QString::number(ri[0], 'f', 2));
    ui->r2->setText(QString::number(ri[1], 'f', 2));
    ui->r3->setText(QString::number(ri[2], 'f', 2));
    ui->r4->setText(QString::number(ri[3], 'f', 2));
    ui->r5->setText(QString::number(ri[4], 'f', 2));
    ui->r6->setText(QString::number(ri[5], 'f', 2));
    ui->r7->setText(QString::number(ri[6], 'f', 2));
    ui->r8->setText(QString::number(ri[7], 'f', 2));
    ui->r9->setText(QString::number(ri[8], 'f', 2));
    ui->r10->setText(QString::number(ri[9], 'f', 2));
    ui->r11->setText(QString::number(ri[10], 'f', 2));
    ui->r12->setText(QString::number(ri[11], 'f', 2));
    ui->r13->setText(QString::number(ri[12], 'f', 2));
    ui->r14->setText(QString::number(ri[13], 'f', 2));
}

void MeasureView::initCqsTab(Cqs &cqs)
{
    ui->cqsSamples->setReference(cqs.refSamXYZ(), cqs.referenceChroma().xyz());
    ui->cqsSamples->setTest(cqs.testSamXYZ(), cqs.testChroma().xyz());

    ui->cqsBarChart->setData(cqs.qi());
    ui->cqsBarChart->setYAxisTitle("Qi");
    ui->cqsBarChart->setXValues(QStringList({ "Q1", "Q2", "Q3", "Q4", "Q5", "Q6", "Q7", "Q8", "Q9", "Q10", "Q11", "Q12", "Q13", "Q14", "Q15" }));
    ui->cqsBarChart->setA(cqs.qa(), "Qa");

    QVector<qreal> qi = cqs.qi();

    ui->qa->setText(QString::number(cqs.qa(), 'f', 2));
    ui->q1->setText(QString::number(qi[0], 'f', 2));
    ui->q2->setText(QString::number(qi[1], 'f', 2));
    ui->q3->setText(QString::number(qi[2], 'f', 2));
    ui->q4->setText(QString::number(qi[3], 'f', 2));
    ui->q5->setText(QString::number(qi[4], 'f', 2));
    ui->q6->setText(QString::number(qi[5], 'f', 2));
    ui->q7->setText(QString::number(qi[6], 'f', 2));
    ui->q8->setText(QString::number(qi[7], 'f', 2));
    ui->q9->setText(QString::number(qi[8], 'f', 2));
    ui->q10->setText(QString::number(qi[9], 'f', 2));
    ui->q11->setText(QString::number(qi[10], 'f', 2));
    ui->q12->setText(QString::number(qi[11], 'f', 2));
    ui->q13->setText(QString::number(qi[12], 'f', 2));
    ui->q14->setText(QString::number(qi[13], 'f', 2));
    ui->q15->setText(QString::number(qi[14], 'f', 2));

}

void MeasureView::initTm3015(Tm3015 &tm3015)
{
    ui->tmBinBarChart->setData(tm3015.binRf());
    ui->tmBinBarChart->setYAxisTitle("Rf");
    QStringList titles;
    for (int i = 1; i <= 16; i++)
        titles.append(QString::number(i));

    ui->tmBinBarChart->setXValues(titles);
    ui->tmBinBarChart->setXAxisTitle("Hue bin");

    QVector<qreal> rfBin = tm3015.binRf();
    ui->bin1->setText(QString::number(rfBin[0], 'f', 2));
    ui->bin2->setText(QString::number(rfBin[1], 'f', 2));
    ui->bin3->setText(QString::number(rfBin[2], 'f', 2));
    ui->bin4->setText(QString::number(rfBin[3], 'f', 2));
    ui->bin5->setText(QString::number(rfBin[4], 'f', 2));
    ui->bin6->setText(QString::number(rfBin[5], 'f', 2));
    ui->bin7->setText(QString::number(rfBin[6], 'f', 2));
    ui->bin8->setText(QString::number(rfBin[7], 'f', 2));
    ui->bin9->setText(QString::number(rfBin[8], 'f', 2));
    ui->bin10->setText(QString::number(rfBin[9], 'f', 2));
    ui->bin11->setText(QString::number(rfBin[10], 'f', 2));
    ui->bin12->setText(QString::number(rfBin[11], 'f', 2));
    ui->bin13->setText(QString::number(rfBin[12], 'f', 2));
    ui->bin14->setText(QString::number(rfBin[13], 'f', 2));
    ui->bin15->setText(QString::number(rfBin[14], 'f', 2));
    ui->bin16->setText(QString::number(rfBin[15], 'f', 2));

    ui->rf->setText(QString::number(tm3015.rf(), 'f', 2));
    ui->rg->setText(QString::number(tm3015.rg(), 'f', 2));
}

void MeasureView::initWindows()
{
    {
        CqsIcon *iconA = new CqsIcon(CqsIcon::IconA);
        iconA->setData(m_cqs.qi());
        m_iconAWindow = iconA;
    }

    {
        CqsIcon *iconB = new CqsIcon(CqsIcon::IconB);
        iconB->setData(m_cqs.chromaRatio());
        m_iconBWindow = iconB;
    }


    {
        RgVsRfPlot *plot = new RgVsRfPlot();
        plot->setRgRf(m_tm30.rg(), m_tm30.rf());

        ProportionalWidget *window = new ProportionalWidget();
        window->setBackgroundColor(QColor(Qt::white));
        window->setWidget(plot);
        window->resize(500, 500);
        window->setMinimumSize(400, 400);
        m_rfrgWindow = window;
    }


    {
        TmDistortionPlot *plot = new TmDistortionPlot(m_tm30.refBinDist(), m_tm30.testBinDist());

        ProportionalWidget *window = new ProportionalWidget();
        window->setBackgroundColor(QColor(Qt::white));
        window->setWidget(plot);
        window->resize(500, 500);
        window->setMinimumSize(400, 400);
        m_distortionWindow = window;
    }
}

void MeasureView::closeEvent(QCloseEvent *event)
{
    if (m_saved)
        event->accept();
    else
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Measure is not saved.");
        msgBox.setInformativeText("Do you want to save measure?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        int ret = msgBox.exec();
        switch (ret)
        {
            case QMessageBox::Save:
                on_actionSave_triggered();
                if (m_saved)
                    event->accept();
                else
                    event->ignore();
                break;

            case QMessageBox::Discard:
                event->accept();
                break;

            case QMessageBox::Cancel:
            default:
                event->ignore();
                break;
        }
    }
}

void MeasureView::on_rfVsRg_clicked()
{
    m_rfrgWindow->show();
    m_rfrgWindow->activateWindow();
}

void MeasureView::on_tmDistortion_clicked()
{
    m_distortionWindow->show();
    m_distortionWindow->activateWindow();
}

void MeasureView::on_cqsIconA_clicked()
{
    m_iconAWindow->show();
    m_iconAWindow->activateWindow();
}

void MeasureView::on_cqsIconB_clicked()
{
    m_iconBWindow->show();
    m_iconBWindow->activateWindow();
}

void MeasureView::on_actionCSV_export_raw_triggered()
{
    CsvFileDialog *dialog = new CsvFileDialog("Export to csv (RAW data)", QFileDialog::AcceptSave);
    dialog->show();
    connect(dialog, SIGNAL(csvSelected(QString,QChar,QChar)), this, SLOT(exportCsvRaw(QString,QChar,QChar)));
}

void MeasureView::on_actionCSV_export_triggered()
{
    CsvFileDialog *dialog = new CsvFileDialog("Export to csv (360 nm to 830 nm, 5 nm steps)", QFileDialog::AcceptSave);
    dialog->show();
    connect(dialog, SIGNAL(csvSelected(QString,QChar,QChar)), this, SLOT(exportCsv(QString,QChar,QChar)));
}

void MeasureView::on_actionSave_triggered()
{
    SaveDialog dialog(this);
    if (dialog.exec() == QDialog::Accepted)
    {
        SpectrumChart *chart = new SpectrumChart();
        chart->setData(m_splinedData);
        chart->setBackgroundBrush(Qt::white);
        chart->setMaxY(1);
        chart->axisX()->setGridLineVisible(false);
        chart->axisY()->setGridLineVisible(false);
        chart->axisX()->setTitleVisible(false);
        chart->axisY()->setTitleVisible(false);
        chart->setFixedSize(263, 125);
        chart->chart()->setMargins(QMargins(0, 0, 0, 0));

        QPixmap img = chart->grab(QRect(39, 16, 200, 80));

        delete chart;

        MeasureRow row = MeasureRow(dialog.getName(), img);
        MeasureManager::insertMeasure(row, m_splinedData);

        setMeasureName(dialog.getName());
        m_saved = true;
    }
}

QString MeasureView::getMeasureName() const
{
    return m_measureName;
}

void MeasureView::setMeasureName(const QString &value)
{
    m_measureName = value;
    setWindowTitle("[" + m_measureName + "] - Spectr");
    m_rfrgWindow->setWindowTitle("[" + m_measureName + "] - Rg vs Rf plot");
    m_distortionWindow->setWindowTitle("[" + m_measureName + "] - TM 30-15 distortion plot");
    m_iconAWindow->setWindowTitle("[" + m_measureName + "] - CQS icon A");
    m_iconBWindow->setWindowTitle("[" + m_measureName + "] - CQS icon B");
}

void MeasureView::normalizeData()
{
    qreal max = 0;
    for (int i = 0; i < m_splinedData.size(); i++)
    {
        if (m_splinedData[i].y() > max)
            max = m_splinedData[i].y();
    }

    for (int i = 0; i < m_splinedData.size(); i++)
        m_splinedData[i].setY(m_splinedData[i].y() / max);
}
